<?php

/**
 * @see https://github.com/deployphp/deployer/blob/master/recipe/drupal8.php
 * @version ^7
 */

namespace Deployer;

require 'recipe/drupal8.php';

//$env = $_SERVER['CI_COMMIT_REF_NAME'] === 'master' ? 'PROD' : 'STAGING';

/* ==========================================================================
   SERVER CONFIGURATION
   ========================================================================== */
set('ssh_type', 'native');
set('ssh_multiplexing', TRUE);
set('writable_mode', 'chown');
set('http_user', 'www-sync');
set('http_group', 'www-data');

/* ==========================================================================
   PROJECT CONFIGURATION
   ========================================================================== */
set('repository', 'git@gitlab.com:mathieumaingret/d9-starter.git');
set('default_stage', 'staging');

/* ==========================================================================
   CMS CONFIGURATION
   ========================================================================== */
set('drupal_site', 'default');
set('drush_command', '{{bin/php}} -d memory_limit=256M {{release_or_current_path}}/vendor/bin/drush');

add('shared_files', [
  '.env',
  'drush/drush.yml',
  'web/.htaccess',
  'web/sites/{{drupal_site}}/services.yml',
  'web/sites/{{drupal_site}}/settings.local.php'
]);
add('shared_dirs', [
  'web/sites/{{drupal_site}}/files',
  'web/themes/custom/frontend/node_modules',
  'local-storage/private',
  'local-storage/tmp',
]);
set('writable_dirs', [
  'web/sites/{{drupal_site}}/files',
  'local-storage/private',
  'local-storage/tmp',
]);


/* ==========================================================================
   HOSTS DEFINITION
   ========================================================================== */
//host('[]')
//    ->set('labels', ['stage' => '[]'])
//    ->set('branch', '[]')
//    ->setHostname('XXX')
//    ->setRemoteUser('www-sync')
//    ->setDeployPath('/XXX')
////    ->set('bin/php', '/usr/bin/php8.0')
////    ->set('bin/composer', '/usr/bin/php8.0 /usr/bin/composer')
//    ->setForwardAgent(true);

//host($env)
host('staging')
  //  ->set('labels', ['stage' => $env])
  ->set('labels', ['stage' => 'staging'])
  //  ->set('branch', 'master')
  ->set('branch', 'master')
  //  ->setHostname('141.94.26.237')
  ->setHostname('141.94.26.237')
  //  ->setRemoteUser('www-sync')
  ->setRemoteUser('www-sync')
  //  ->setPort('59987')
  ->setPort('59987')
  //  ->setDeployPath('/var/www/tests.mathieumaingret.fr/public_html')
  ->setDeployPath('/var/www/tests.mathieumaingret.fr/public_html')
  ->setForwardAgent(TRUE);

/* ==========================================================================
   WORKFLOW DEFINITION
   ========================================================================== */
task('deploy', [
  // -----  deploy:prepare
  'deploy:info',
  'deploy:setup',
  'deploy:lock',
  'deploy:release',
  'deploy:update_code',
  'deploy:shared',
  'deploy:writable',
  // ----- deploy:custom
  'deploy:vendors',       // custom
  'deploy:node_modules',  // custom
  'drush:cc',             // custom
  'drush:sql-dump',       // custom
  'drush:maintenance_on', // custom
  'drush:updb',           // custom
  'drush:cim',            // custom
//  'drush:translate',      // custom
  'drush:cr',             // custom
  'drush:maintenance_off', // custom
  // -----  deploy:publish
  'deploy:symlink',
  'deploy:unlock',
  'deploy:cleanup',
  'deploy:success',
]);

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

/* ==========================================================================
   CUSTOM TASKS DEFINITION
   ========================================================================== */
desc('Set maintenance mode ON');
task('drush:maintenance_on', function() {
  if (has('release_path')) {
    run('{{drush_command}} maint:set 1');
  }
});

desc('Set maintenance mode OFF');
task('drush:maintenance_off', function() {
  run('{{drush_command}} maint:set 0');
});

desc('Clear drush cache');
task('drush:cc', function() {
  run('{{drush_command}} cc drush -y');
});

desc('Clear all caches with Drush');
task('drush:cr', function() {
  run('{{drush_command}} cr -y');
});

desc('Load gulp dependencies');
task('deploy:node_modules', function() {
  run('cd {{release_path}}/web/themes/custom/frontend && yarn --production=true');
});

desc('Update database');
task('drush:updb', function() {
  run('{{drush_command}} updatedb -y --no-cache-clear');
});

desc('Update translations');
task('drush:translate', function() {
  run('{{drush_command}} locale:check');
  run('{{drush_command}} locale:update');
});

desc('Revert configs');
task('drush:cim', function() {
  run('{{drush_command}} cim -y');
});

desc('Dump database');
task('drush:sql-dump', function() {
  if (has('previous_release')) {
    run('{{drush_command}} sql-dump --gzip --result-file=dump-' . ((int) get('release_name') - 1) . '.sql');
  }
});

desc('Deploy secrets');
task('deploy:secrets', function () {
  upload(getenv('DOTENV'), '{{deploy_path}}/shared/.env');
});

desc('Add tag to branch');
task('git:add-tag', function() {
  $lastCommitId = runLocally('git log --format="%H" -n 1 -b {{branch}}');
  runLocally('git tag {{stage}}-{{release_name}} ' . $lastCommitId . ' && git push origin {{stage}}-{{release_name}}');
});
