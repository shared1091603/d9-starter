import { createRequire } from 'module';
import dartSass from 'node-sass';
import gulpSass from 'gulp-sass';
import gulp from 'gulp';
import dotenv from 'dotenv';
import postcss from 'gulp-postcss';
import cleancss from 'gulp-clean-css';
import clean from 'gulp-clean';
import minify from 'gulp-minify';
import changed from 'gulp-changed';
import iconfont from 'gulp-iconfont';
import iconfontCss from 'gulp-iconfont-css';
import sourcemaps from 'gulp-sourcemaps';
import imagemin, { gifsicle, mozjpeg, optipng, svgo } from 'gulp-imagemin';
import autoprefixer from 'autoprefixer';

const require = createRequire(import.meta.url);

const packagejson = require('./package.json');
const runTimestamp = Math.round(Date.now() / 1000);

const sass = gulpSass(dartSass);

// Load .env file
dotenv.config();
const APP_ENV = process.env.APP_ENV || 'production';
const isDev = APP_ENV === 'local';

/**
 *
 */
function styles(theme) {
  let path = theme === 'backend' ? packagejson.paths.theme_back : packagejson.paths.theme_front;

  if (isDev) {
    return gulp.src(path + '/src/scss/*.scss')
      .pipe(sourcemaps.init())
      .pipe(sass({
        verbose: false
      }).on('error', sass.logError))
      .pipe(postcss([
        autoprefixer({
          Browserslist: ['last 1 version'],
        }),
      ]))
      .pipe(sourcemaps.write('.'))
      // .pipe(cleancss())
      .pipe(gulp.dest(path + '/dist/css'));
  } else {
    return gulp.src(path + '/src/scss/*.scss')
      .pipe(sass({
        outputStyle: 'compressed',
      }))
      .pipe(postcss([
        autoprefixer({
          Browserslist: ['last 1 version'],
        }),
      ]))
      .pipe(cleancss({ level: 2 }))
      .pipe(minify())
      .pipe(gulp.dest(path + '/dist/css'));
  }
}

function styles_backend() {
  return styles('backend');
}

/**
 *
 */
function scripts(theme) {
  let path = theme === 'backend' ? packagejson.paths.theme_back : packagejson.paths.theme_front;

  if (isDev) {
    return gulp.src(path + '/src/js/**/*.js')
      .pipe(gulp.dest(path + '/dist/js'));
  } else {
    return gulp.src(path + '/src/js/**/*.js')
      .pipe(gulp.dest(path + '/dist/js'));
  }
}

function scripts_backend() {
  return scripts('backend');
}

/**
 *
 */
function fonts() {
  return gulp.src(packagejson.paths.theme_front + '/src/fonts/**/*', { encoding: false })
    .pipe(gulp.dest(packagejson.paths.theme_front + '/dist/fonts'));
}

function font_icons() {
  var fontName = 'icons-font';

  return gulp.src(packagejson.paths.theme_front + '/src/images/icons/*.svg', { encoding: false })
    .pipe(iconfontCss({
      fontName: fontName,
      path: packagejson.paths.theme_front + '/src/scss/configs/_fonticon-template.scss',
      targetPath: '../scss/_font-icons.scss',
      fontPath: '../fonts/',
      cacheBuster: runTimestamp,
    }))
    .pipe(iconfont({
      fontName: fontName,
      formats: ['woff', 'woff2'],
      timestamp: runTimestamp,
      normalize: true,
      fontHeight: 1024
    }))
    .pipe(gulp.dest(packagejson.paths.theme_front + '/dist/fonts'));
}

/**
 *
 */
function images() {
  return gulp.src(packagejson.paths.theme_front + '/src/images/**/*', { encoding: false })
    .pipe(imagemin([
      gifsicle({ interlaced: true }),
      mozjpeg({ quality: 75, progressive: true }),
      optipng({ optimizationLevel: 5 }),
      svgo({
        plugins: [
          {
            name: 'removeViewBox',
            active: true,
          },
          {
            name: 'cleanupIDs',
            active: false,
          },
        ],
      }),
    ], { verbose: isDev }))
    .pipe(gulp.dest(packagejson.paths.theme_front + '/dist/images'));
}

/**
 *
 */
function clean_dist() {
  return gulp.src([packagejson.paths.theme_front + '/dist/images/*'])
    .pipe(clean({
      force: true,
      allowEmpty: true,
    }));
}

/**
 *
 */
function watch_files() {
  gulp.watch(packagejson.paths.theme_front + '/src/scss/**/*.scss', styles);
  gulp.watch(packagejson.paths.theme_back + '/src/scss/**/*.scss', styles_backend);
  gulp.watch(packagejson.paths.theme_front + '/src/js/**/*.js', scripts);
  gulp.watch(packagejson.paths.theme_back + '/src/js/**/*.js', scripts_backend);
  gulp.watch(packagejson.paths.theme_front + '/src/fonts/**/*', fonts);
  gulp.watch(packagejson.paths.theme_front + '/src/images/**/*', images);
  gulp.watch(packagejson.paths.theme_front + '/src/images/icons/*', gulp.series(font_icons, styles));
}

/**
 *
 */
function welcome(done) {
  console.debug('env=' + APP_ENV + ';dev_mode=' + isDev);
  done();
}

// Define tasks
const build = gulp.series(welcome, fonts, images, font_icons, gulp.parallel(styles, styles_backend, scripts, scripts_backend));
gulp.task('default', gulp.series(watch_files));

gulp.task('styles', gulp.series(styles, styles_backend));
gulp.task('scripts', gulp.series(scripts, scripts_backend));
gulp.task('assets', gulp.series(fonts, images, font_icons, styles));

gulp.task('watch', gulp.series(welcome, watch_files));
