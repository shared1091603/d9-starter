#
# Drupal10 Makefile v1.1

COMMAND_PREFIX		:= lando
DUMP_DIRECTORY		:= "_sql/"

# Help
.PHONY: help
help:
	@echo "Usage: make <target>"
	@echo ""
	@echo "Available targets:"
	@echo "  start        Start lando and check diff"
	@echo "  dump         Dump database to $(DUMP_DIRECTORY) directory"
	@echo "  up           Update composer dependencies and launch migrations"
	@echo "  upnode       Update node dependencies"
	@echo "  upfront      Update front theme dependencies"
	@echo "  translate    Import custom translations"
	@echo "  phpcs        Run php inspections"

# Start lando
.PHONY: start
start:
	$(COMMAND_PREFIX) start
	$(COMMAND_PREFIX) composer install
	$(COMMAND_PREFIX) drush config:status

# Export database
.PHONY: dump
dump:
	@echo "-------- Exporting database to $(DUMP_DIRECTORY)"
	@[ -d $(DUMP_DIRECTORY) ] || mkdir -v $(DUMP_DIRECTORY)
	@cd $(DUMP_DIRECTORY) && $(COMMAND_PREFIX) db-export

# Update Drupal
.PHONY: up
up:
	@make dump
	@echo "-------- Updating Composer"
	$(COMMAND_PREFIX) composer update
	@echo "-------- Upgrading database"
	$(COMMAND_PREFIX) drush updb --no-cache-clear --yes
	@echo "-------- Updating translations"
	$(COMMAND_PREFIX) drush locale-update -y
	@echo "-------- Clearing caches"
	$(COMMAND_PREFIX) drush cr -y
	@make dump

# Update node dependencies
.PHONY: upnode
upnode:
	@echo "-------- Updating Node dependencies"
	$(COMMAND_PREFIX) npm install

# Update theme dependencies
.PHONY: upfront
upfront:
	@echo "-------- Updating Theme libraries"
	$(COMMAND_PREFIX) yarn install --cwd ./web/themes/custom/frontend
	$(COMMAND_PREFIX) yarn upgrade --cwd ./web/themes/custom/frontend

# Update site translations
.PHONY: translate
translate:
	@echo "-------- Updating custom site translations"
	$(COMMAND_PREFIX) drush csv2po
	$(COMMAND_PREFIX) drush locale:check
	$(COMMAND_PREFIX) drush locale:update
	$(COMMAND_PREFIX) drush cr

# Inspect PHP using squizlabs/php_codesniffer library
# @link https://github.com/HBFCrew/lando-docs-examples/blob/master/Drupal-PHPStorm-Lando-Setting-Code-Sniffer-Debugging.md
.PHONY: phpcs
phpcs:
	@echo "-------- Executing Drupal phpcs"
	$(COMMAND_PREFIX) phpcs --standard=Drupal --exclude=Drupal.Arrays.Array,Drupal.WhiteSpace.ScopeIndent --extensions=php,module,inc,install,test,profile,theme,css,info,txt,md,yml web/modules/custom

