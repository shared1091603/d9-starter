# TODO

## Todo starter

### Todo dev/devOps

- [ ] Dépot github
- [ ] Deploy.php ft Gitlab .env
- [ ] Doc des modules avec Swagger (entityfield, ...)
- [ ] Breadcrumb auto si modification des urls ?
- [ ] Revoir le binding drup / drup_site
- [ ] Rename Helper => Utility
- [ ] self => static
- [ ] .editorconfig
- [ ] phpstan correctifs

### Todo test

- [ ] assert

### Todo front

- [ ] Trouver Lib Slider
- [ ] Refonte des SASS
- [ ] Guide de style automatisé avec les sass/modules

## Todo modules
- [ ] Drup Report (github)
- [ ] Drup Paragraph Model (github)
- [ ] Drup Entity Overview (github)
