<?php

(new Symfony\Component\Dotenv\Dotenv())->bootEnv(DRUPAL_ROOT.'/../.env');

$env = $_ENV['APP_ENV'] ?: 'local';
$site_id = explode('/', $site_path)[1];

/* ==========================================================================
   DATABASES + CACHES
   ========================================================================== */
$databases['default']['default'] = [
  'database' => $_ENV['DB_DATABASE'],
  'username' => $_ENV['DB_USER'],
  'password' => $_ENV['DB_PASSWORD'],
  'prefix' => 'hD8k_',
  'host' => $_ENV['DB_HOST'],
  'port' => $_ENV['DB_PORT'],
  'isolation_level' => 'READ COMMITTED',
  'driver' => 'mysql',
  'namespace' => 'Drupal\\mysql\\Driver\\Database\\mysql',
  'autoload' => 'core/modules/mysql/src/Driver/Database/mysql/',
];

//
if (!empty($_ENV['REDIS_HOST'])) {
  $settings['cache']['default'] = 'cache.backend.redis';
  $settings['redis.connection']['host'] = $_ENV['REDIS_HOST'];
  $settings['redis.connection']['port'] = $_ENV['REDIS_PORT'];
  $settings['cache_prefix'] = $env.'_'.$site_id.'_';
}

/* ==========================================================================
   CONFIGS + DIRECTORIES
   ========================================================================== */
$settings['config_sync_directory'] = '../local-storage/config/' . $site_id;
$settings['file_private_path'] = '../local-storage/private/' . $site_id;
$settings['file_temp_path'] = '../local-storage/tmp/' . $site_id;

$settings['container_yamls'][] = DRUPAL_ROOT.'/sites/default/services.yml';

$settings['hash_salt'] = 'WJRRgOYmTyHNS5PPLtfveaGYQBVuVCE3JmDI9NOOtCikdnRj8yYpKy885OTSA5B_CU-Hhn47g';
$settings['css_js_aggregate_hash_query_string'] = TRUE;
$settings['state_cache'] = TRUE;

$settings['trusted_host_patterns'] = $_ENV['TRUSTED_HOST_PATTERNS'] ? explode(' ', $_ENV['TRUSTED_HOST_PATTERNS']) : [];

$google_sheet_id = '';
$settings['drup_csv2po']['csv_remote_url'] = 'https://docs.google.com/spreadsheets/d/'.$google_sheet_id.'/gviz/tq?tqx=out:csv';

/* ==========================================================================
   BY ENVIRONNEMENT
   ========================================================================== */
switch ($env) {
  /* Production
     ========================================================================== */
  case 'prod':
    $config['config_split.config_split.prod']['status'] = TRUE;
    $settings['trusted_host_patterns'] = [];
    break;

  /* Staging
   ========================================================================== */
  case 'staging':
    $config['config_split.config_split.preprod']['status'] = TRUE;
    $settings['trusted_host_patterns'] = [];
    break;

  /* Staging
========================================================================== */
  case 'inte':
    $config['config_split.config_split.inte']['status'] = TRUE;
    $settings['trusted_host_patterns'] = [];
    break;

  /* Dev
   ========================================================================== */
  case 'local':
    // configs dev + ignore split
    $config['config_split.config_split.dev']['status'] = TRUE;
    $settings['config_ignore_deactivate'] = TRUE;

    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    $settings['container_yamls'][] = DRUPAL_ROOT.'/sites/development.services.yml';
    $settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';
    $settings['cache']['bins']['render'] = 'cache.backend.null';
    $settings['cache']['bins']['page'] = 'cache.backend.null';
    break;
}

/* Custom local overrides
   ========================================================================== */
if (file_exists($app_root.'/'.$site_path.'/settings.local.php')) {
  include $app_root.'/'.$site_path.'/settings.local.php';
}
