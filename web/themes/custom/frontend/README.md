## Init theme

### Images

- [ ] logo.svg
- [ ] logo.png + logo-mail.png
- [ ] favicons : https://realfavicongenerator.net/

### Styles - fonts

- [ ] https://gwfh.mranftl.com/fonts/roboto?subsets=latin (path = ../../fonts/)

### Styles - variables

tips & exercises to relieve my symptoms