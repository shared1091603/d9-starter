<?php

/**
 * @inheritdoc
 */
function frontend_preprocess_container(&$variables) {
  if (isset($variables['attributes']['id']) && $variables['attributes']['id'] === 'edit-actions') {
    if (isset($variables['element']['submit']['#id'])) {
      $variables['attributes']['id'] = $variables['attributes']['data-drupal-selector'] = $variables['element']['submit']['#id'].'-actions';
    }
  }
}

/**
 * @inheritdoc
 */
function frontend_theme_suggestions_input_alter(array &$suggestions, array $variables) {
  $element = $variables['element'];

  // Force tous les #type => 'submit' en <button>
  if (isset($element['#type']) && $element['#type'] === 'submit' && (!isset($element['#button_type']))) {
    $suggestions[] = 'input__submit_custom';
  }
  // Ou tout élément forcé en bouton
  if (isset($element['#button_type']) && $element['#button_type'] === 'button') {
    $suggestions[] = 'input__submit_custom';
  }
}

/**
 * @inheritdoc
 */
function frontend_preprocess_input(array &$variables) {
  //    switch ($variables['attributes']['type']) {
  //        case 'submit':
  //            $variables['attributes']['class'][] = 'btn btn---primary';
  //            break;
  //    }

  // btn---[class] on submit
  if ($variables['theme_hook_original'] === 'input__submit') {
    $buttonType = NULL;

    if (array_key_exists('#button_type', $variables['element']) && !empty($variables['element']['#button_type'])) {
      $buttonType = $variables['element']['#button_type'];
    }
    else {
      $buttonType = 'submit';
    }

    // Exception : btn supprimer fichier uploadé ou déjà class btn
    if (in_array('remove_button', $variables['element']['#parents']) || in_array('btn', $variables['element']['#attributes']['class'])) {
      $buttonType = NULL;
    }
    // Spécific : btn add more webform
    if (isset($variables['element']['#submit'][0][0]) && $variables['element']['#submit'][0][0] === 'Drupal\webform\Element\WebformCustomComposite') {
      $buttonType = 'more';
    }

    if (isset($buttonType)) {
      $variables['attributes']['class'][] = 'btn';

      // Gestion déjà une classe btn--
      $addClass = TRUE;
      foreach ($variables['element']['#attributes']['class'] as $class) {
        if (strpos($class, 'btn--') !== FALSE) {
          $addClass = FALSE;
          break;
        }
      }
      if ($addClass) {
        $variables['attributes']['class'][] = 'btn--'.$buttonType;

        if ($buttonType === 'submit') {
          $variables['attributes']['class'][] = 'btn--primary';
        }
      }
    }

    // Force les '#type' => 'button en <button type="button"></button>
    if ($variables['element']['#type'] === 'button') {
      $variables['attributes']['type'] = 'button';
    }
  }
}
