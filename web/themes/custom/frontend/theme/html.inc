<?php

use Drupal\Component\Utility\Html;
use Drupal\Core\Template\Attribute;
use Drupal\drup\Helper\DrupRequest;
use Drupal\drup_site\Utility\SEOUtility;

/**
 * @inheritdoc
 */
function frontend_preprocess_html(array &$variables) {
  /** @var \Drupal\drup_settings\DrupSettings $drupSettings */
  $drupSettings = \Drupal::service('drup.settings');

  /** @var \Drupal\drup_router\DrupRouter $drupRouter */
  $drupRouter = \Drupal::service('drup.router');
  $systemRouteName = DrupRequest::getRouteName();

  // JS variables
  $variables['#attached']['drupalSettings']['pathToTheme'] = '/'.\Drupal::service('extension.list.theme')->getPath('frontend');

  // Body classes
  if (!($variables['attributes'] instanceof Attribute)) {
    $variables['attributes'] = new Attribute();
  }

  $variables['attributes']->addClass('body');
  $variables['attributes']->addClass('is-header-sticky');

  if ($variables['logged_in']) {
    $variables['attributes']->addClass('is-logged-in');
  }
  if ($variables['root_path'] === FALSE) {
    $variables['attributes']->addClass('is-front');
  }
  else {
    /** @var \Drupal\drup\DrupPageEntity $drupPageEntity */
    $drupPageEntity = \Drupal::service('drup.page_entity');

    if (($entityType = $drupPageEntity->getEntityType()) && ($bundle = $drupPageEntity->getBundle())) {
      $variables['attributes']->addClass([
        'entity--'.Html::cleanCssIdentifier($entityType),
        'entity--'.Html::cleanCssIdentifier($entityType).'--'.Html::cleanCssIdentifier($bundle),
      ]);

      if (\Drupal\drup_site\Utility\ContentUtility::hasBanner($drupPageEntity->getEntity())) {
        $variables['attributes']->addClass('l-content-banner');
      }
    }
  }

  if ($routeName = $drupRouter->getName()) {
    $variables['#attached']['drupalSettings']['currentRoute'] = $routeName;
    $variables['attributes']->addClass('route--'.Html::cleanCssIdentifier($routeName));
  }
  if ($systemRouteName) {
    $variables['attributes']->addClass('route--'.Html::cleanCssIdentifier(str_replace('.', '-', $systemRouteName)));
  }

  // User role @see module drup_admin_toolbar

  // Maintenance
  if ((bool) \Drupal::state()->get('system.maintenance_mode') === TRUE) {
    $variables['attributes']->addClass('maintenance-page');
  }

  // Translations
  $variables['#attached']['drupalSettings']['frontendTranslations'] = [
    //    'view_more' => t('View more'),
  ];

  // Code tracking
  $variables['#attached']['drupalSettings']['google_tag'] = $drupSettings->getValue('site_tag_manager');

  // Favicons
  SEOUtility::addFavicons($variables, [
    'color_mask_icon' => '#5bbad5',
    'color_msapplication' => '#da532c',
    'color_theme' => '#ffffff',
  ], 1);
}
