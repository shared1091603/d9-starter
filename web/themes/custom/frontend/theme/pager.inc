<?php

/**
 * Implements hook_preprocess_pager().
 */
function frontend_preprocess_pager(&$variables) {
  unset($variables['items']['last'], $variables['items']['first']);

  if (isset($variables['items']['next'])) {
    $variables['items']['next']['attributes']->addClass(['btn', 'l-small']);
    $variables['items']['next']['text'] = t('Next');
  }
  if (isset($variables['items']['previous'])) {
    $variables['items']['previous']['attributes']->addClass(['btn', 'l-small']);
    $variables['items']['previous']['text'] = t('Previous');
  }
}
