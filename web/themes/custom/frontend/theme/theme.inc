<?php

/**
 * @return array
 */
function frontend_theme() {
  $themes = [];
  $themePath = Drupal::theme()->getActiveTheme()->getPath();

  $themes['frontend_accordions'] = [
    'path' => $themePath.'/templates/partials',
    'template' => 'accordion',
    'preprocess functions' => ['_frontend_preprocess_accordions'],
    'variables' => [
      'attributes' => [],
      'items' => [],
    ],
  ];

  return $themes;
}

/**
 * @param $variables
 *
 * @return void
 */
function _frontend_preprocess_accordions(&$variables) {
  $variables['attributes'] = new \Drupal\Core\Template\Attribute($variables['attributes']);
  $variables['#attached']['library'][] = 'frontend/utility-accordions';
}
