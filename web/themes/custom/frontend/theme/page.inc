<?php

use Drupal\Core\Template\Attribute;
use Drupal\drup\Entity\EntityField;
use Drupal\drup\Entity\Node;
use Drupal\drup\Helper\DrupRequest;
use Drupal\drup_site\Utility\ContentUtility;

/**
 * Implements hook_preprocess_page() for page.html.twig.
 */
function frontend_preprocess_page(array &$variables) {
  /** @var \Drupal\drup_settings\DrupSettings $drupSettings */
  $drupSettings = \Drupal::service('drup.settings');

  /** @var \Drupal\drup\DrupPageEntity $drupPageEntity */
  $drupPageEntity = \Drupal::service('drup.page_entity');

  /** @var \Drupal\drup_router\DrupRouter $drupRouter */
  $drupRouter = \Drupal::service('drup.router');
  $drupRouteName = $drupRouter->getName();

  $systemRouteName = DrupRequest::getRouteName();
  $isFront = DrupRequest::isFront();

  $contentRow = TRUE;

  $variables['page_attributes'] = new Attribute([
    'id' => 'page',
    'class' => [
      'page',
    ],
  ]);
  $variables['content_banner_attributes'] = new Attribute([
    'id' => 'content-banner',
    'class' => [
      'content-header-banner',
    ],
  ]);
  $variables['content_before_attributes'] = new Attribute([
    'id' => 'content-before',
    'class' => [
      'content-before',
    ],
  ]);
  $variables['content_attributes'] = new Attribute([
    'id' => 'content',
    'class' => [
      'content',
    ],
  ]);
  $variables['content_body_attributes'] = new Attribute([
    'id' => 'content-body',
    'class' => [
      'content-body',
    ],
  ]);

  /**
   * Load libraries
   */
  if ($isFront) {
    $variables['#attached']['library'][] = 'frontend/theme-front';
  }
  else {
    if ($drupRouteName === 'contact') {
      $variables['page']['content'][] = [
        '#type' => 'webform',
        '#webform' => 'contact',
      ];
      $variables['#attached']['library'][] = 'frontend/theme-contact';
    }
  }

  // Bannière header
  if ($mediaBanner = ContentUtility::getBanner($drupPageEntity->getEntity())) {
    $bannerStyle = 'fluid_banner';
    $variables['content_banner'] = $mediaBanner->renderImage($bannerStyle, ['alt' => '']);
    $variables['content_before_attributes']->addClass([
      'l-banner-bg',
      'theme-white-contrast',
    ]);
  }
  else {
    //
    $variables['content_before_attributes']->addClass([]);
  }

  /**
   * Node fields
   */
  $entity = $drupPageEntity->getEntity();
  if ($entity instanceof \Drupal\Core\Entity\ContentEntityBase) {
    if ($entity->hasField(ContentUtility::$fieldParagraphs) && !$entity->get(ContentUtility::$fieldParagraphs)->isEmpty()) {
      $contentRow = FALSE;
    }

    if ($entity instanceof Node) {
      $entityField = $entity->entityField();
      // ...
    }
  }

  /* 404 / 403
    ========================================================================== */
  elseif (\Drupal::request()->attributes->get('_route') === 'system.404') {
    $variables['page']['content'] = [
      '#theme' => 'drup_content_404',
      '#data' => [
        'contact_url' => $drupRouter->getUrl('contact')?->toString(),
      ],
    ];
  }
  elseif (\Drupal::request()->attributes->get('_route') === 'system.403') {
    $variables['page']['content'] = [
      '#theme' => 'drup_content_403',
    ];
  }

  if ($contentRow === TRUE) {
    $variables['content_body_attributes']->addClass(['row']);
  }
}

/**
 * Implements hook_preprocess_page_title().
 *
 * @param  array  $variables
 */
function frontend_preprocess_page_title(array &$variables) {
  /** @var \Drupal\drup\DrupPageEntity $drupPageEntity */
  $drupPageEntity = \Drupal::service('drup.page_entity');

  /** @var \Drupal\drup_router\DrupRouter $drupRouter */
  $drupRouter = \Drupal::service('drup.router');

  $variables['title_attributes'] = new Attribute([
    'class' => ['content-title'],
  ]);
  $variables['title_tag'] = 'h1';

  //$variables['title']['#allowed_tags'][] = 'br';

  $variables['subtitle'] = NULL;
  $variables['subtitle_attributes'] = new Attribute(['class' => []]);

  $variables['categories'] = [];
  $variables['thumbnail'] = [];
  $variables['content'] = [];

  if (($entity = $drupPageEntity->getEntity()) && $entity instanceof \Drupal\Core\Entity\ContentEntityBase) {
    $nodeType = $drupPageEntity->getBundle();
    $entityField = new EntityField($entity);

    if ($entity instanceof Node) {
      // Sous titre / Introduction
      if ($variables['subtitle'] = ContentUtility::getSubtitle($entity)) {
        $variables['subtitle_attributes'] = new Attribute([
          'class' => [],
        ]);
      }
    }
  }
}

/**
 * Implements hook_theme_suggestions_page_alter().
 */
function frontend_theme_suggestions_page_alter(array &$suggestions, array $variables) {
  /** @var \Drupal\drup\DrupPageEntity $drupPageEntity */
  $drupPageEntity = Drupal::service('drup.page_entity');

  //-- accesses the content type on page landing
  if ($drupPageEntity->getBundle() !== NULL) {
    $suggestions[] = 'page__'.$drupPageEntity->getBundle();
  }
  elseif (Drupal::request()->attributes->get('_route') === 'system.404') {
    $suggestions[] = 'page__404';
  }
  elseif (Drupal::request()->attributes->get('_route') === 'system.403') {
    $suggestions[] = 'page__403';
  }

  /** @var \Drupal\drup_router\DrupRouter $drupRouter */
  $drupRouter = Drupal::service('drup.router');
  if ($route = $drupRouter->getName()) {
    $suggestions[] = 'page__'.$route;
  }
}
