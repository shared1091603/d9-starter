<?php

use Drupal\Component\Utility\Html;

/**
 * @inheritdoc
 */
function frontend_theme_suggestions_field_alter(array &$suggestions, array $variables) {
  $fieldName = $variables['element']['#field_name'];

  // Template field like slider
  if ($variables['element']['#is_multiple'] && ($fieldName === 'field_items')) {
    $suggestions[] = 'field__slider';
    $suggestions[] = 'field__'.$fieldName.'__slider';
  }
}

/**
 * @inheritdoc
 */
function frontend_preprocess_field(array &$variables, $hook) {
  $fieldName = $variables['element']['#field_name'];
  $fieldType = $variables['element']['#field_type'];

  // If true, Subitems without <div>
  $variables['multiple_simple_layout'] = FALSE;

  $fieldNamespace = 'item';

  if ($fieldType == 'text_long') {
    $variables['attributes']['class'][] = 'ck-content';
  }

  /* Field from paragraph
     ========================================================================== */
  if ($variables['entity_type'] === 'paragraph') {
    $variables['attributes']['data-paragraph-id'] = $variables['element']['#object']?->id();
    $variables['multiple_simple_layout'] = TRUE;

    if (str_contains($fieldName, 'field_image') || str_contains($fieldName, 'field_media') || str_contains($fieldName, 'field_medias')) {
      $variables['attributes']['class'][] = 'item-media';
    }
  }

  $variables['attributes']['class'][] = Html::cleanCssIdentifier(str_replace('field', $fieldNamespace, $fieldName));
}
