<?php

use Drupal\drup\Views\DrupViews;
use Drupal\drup_site\DrupSiteForm;

/**
 * @inheritdoc
 */
function frontend_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
  // WEBFORM (CONTACT)
  if (str_contains($form_id, 'webform_submission_')) {
    $form['#attributes']['class'][] = 'form'; // Ne pas remplacer les classes par défaut, elles sont utilisées par webform (conditions)
    $form['#attributes']['class'][] = 'form--webform';
    $form['#attributes']['class'][] = 'form--'.str_replace('_', '-', $form['#webform_id']);

    if (isset($form['elements']['rgpd'])) {
      $form['elements']['rgpd']['#title'] = \Drupal\drup_site\Utility\FormUtility::getGDPRCheckboxLabel('contact');
    }
    if (isset($form['elements']['actions'])) {
      $form['elements']['actions']['#suffix'] = '<p class="form-mention-required">* Champs obligatoire</p>';
    }

    // Icon + type button
    $form['actions']['submit']['#button_type'] = 'button';
  }

  // LOGIN + PASSWORD
  elseif (\in_array($form_id, ['user_login_form', 'user_pass'])) {
    $form['#attributes']['class'][] = 'form';
    $form['#attributes']['class'][] = 'form--login';

    // Reset password
    $form['actions']['reset'] = [
      '#type' => 'markup',
      '#markup' => \Drupal\Core\Link::createFromRoute(t($form_id === 'user_pass' ? 'Cancel' : 'Reset password'), 'user.'.($form_id === 'user_pass' ? 'login' : 'pass'))->toString(),
      '#prefix' => '<div class="js-form-item form-item js-form-type-markup form-item-reset js-form-item-reset">',
      '#suffix' => '</div>',
    ];

    // Remove useless description
    unset(
      $form['name']['#description'],
      $form['pass']['#description']
    );
  }
}

/**
 * @inheritdoc
 */
function frontend_form_views_exposed_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
  /** @var \Drupal\views\ViewExecutable $viewExecutable */
  $viewExecutable = $form_state->get('view');
  /** @var \Drupal\views\Entity\View $view */
  $view = $form_state->get('view')->storage;

  $form['#attributes']['class'][] = 'form';
  $form['#attributes']['class'][] = 'form--filters-primary';
  $form['#attached']['library'][] = 'frontend/utility-form-filters';

  // Submit
  $form['actions']['submit']['#button_type'] = 'primary';
  $form['actions']['submit']['#value'] = t('Search', [], ['context' => '']);

  //
  if ($form['#id'] === '') {
  }

  // Tags (JS)
  $form['form-tags'] = [
    '#type' => 'container',
    '#weight' => 100,
    '#attributes' => [
      'class' => 'form-tags',
    ],
  ];
}
