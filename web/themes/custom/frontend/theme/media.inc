<?php

use Drupal\Component\Utility\Html;

/**
 * @inheritdoc
 */
function frontend_preprocess_media(&$variables) {
  /** @var \Drupal\drup\Entity\Media $media */
  $media = $variables['media'];
  $type = $media->bundle();
  $viewMode = $variables['view_mode'];

  $variables['attributes']['class'][] = 'media';
  $variables['attributes']['class'][] = 'media--'.Html::cleanCssIdentifier($type);
  $variables['attributes']['class'][] = 'l-'.Html::cleanCssIdentifier($viewMode);

  /* Global
     ========================================================================== */
  if ($legend = $media->entityField()->getValue('legend', 'value')) {
    $variables['attributes']['class'][] = 'l-description';
    $variables['legend'] = nl2br($legend);
  }

  /* Documents
     ========================================================================== */
  if ($type === 'document') {
    if ($file = $media->getReferencedFile()) {
      $variables['document_info'] = $file->getInfo();
      $variables['document_url'] = $file->getUrl();
      $variables['document_uri'] = $file->uri();
      $variables['document_name'] = $variables['name'].' ('.$file->getInfo()['size_human'].')';
    }
  }

  /* Vidéos Youtube
     ========================================================================== */
  elseif ($type === 'video_external') {
    $variables['video_url'] = $media->hasField('field_media_oembed_video') ? $media->get('field_media_oembed_video')->value : NULL;
  }

  /* Images
     ========================================================================== */
  elseif ($type === 'image') {
  }
}
