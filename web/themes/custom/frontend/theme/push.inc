<?php

function frontend_preprocess_content_push(&$variables) {
  /** @var \Drupal\drup_push\Entity\ContentPush $entity */
  $entity = $variables['elements']['#content_push'];

  if ($entity->get('field_image')->isEmpty()) {
    $variables['attributes']['class'][] = 'theme-background-primary';
  }
  else {
    $variables['attributes']['class'][] = 'l-image';
    $variables['attributes']['class'][] = 'theme-white-contrast';
  }
}
