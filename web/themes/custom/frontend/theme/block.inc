<?php

use Drupal\gm\GmPageEntity;

/**
 * @inheritdoc
 */
function frontend_preprocess_block(&$variables) {
  $pluginId = $variables['base_plugin_id'];
  $derivativeId = $variables['derivative_plugin_id'];
  $blockId = $variables['elements']['#id'];

  $variables['attributes']['class'][] = 'block';
  $variables['attributes']['class'][] = 'block--'.\Drupal\Component\Utility\Html::cleanCssIdentifier($blockId);

  if (in_array($pluginId, [
    'system_menu_block',
    'menu_block',
    'language_block',
  ])) {
    $variables['attributes']['class'][] = 'nav';

    $classes = [
      'main_menu' => 'main',
      'menu_footer' => 'footer',
      'languages_selector' => 'languages',
    ];
    if (isset($classes[$blockId])) {
      $variables['attributes']['class'][] = 'nav--'.\Drupal\Component\Utility\Html::cleanCssIdentifier($classes[$blockId]);
      $variables['attributes']['id'] = 'nav-'.\Drupal\Component\Utility\Html::cleanCssIdentifier($classes[$blockId]);
    }
    else {
      $variables['attributes']['class'][] = 'nav--'.\Drupal\Component\Utility\Html::cleanCssIdentifier($derivativeId);
    }
  }

  if ($pluginId === 'system_breadcrumb_block') {
    $variables['attributes']['class'][] = 'row';
    $variables['attributes']['class'][] = 'row--big';
  }
  elseif ($pluginId === 'page_title_block') {
    $variables['attributes']['class'][] = 'row';
    $variables['attributes']['class'][] = 'row--big';
  }
}
