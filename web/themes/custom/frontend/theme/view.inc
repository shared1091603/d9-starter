<?php

use Drupal\Component\Utility\Html;

/**
 * Preprocess View
 *
 * @param $variables
 */
function frontend_preprocess_views_view(&$variables) {
  $variables['attributes']['class'][] = 'views';
  $variables['attributes']['class'][] = 'views--'.Html::cleanCssIdentifier($variables['id']);
  $variables['attributes']['class'][] = 'views--'.Html::cleanCssIdentifier($variables['id']).'--'.Html::cleanCssIdentifier($variables['display_id']);

  if (!empty($variables['exposed'])) {
    $variables['attributes']['class'][] = 'l-filters';
  }

  /** @var \Drupal\drup_router\DrupRouter $drupRouter */
  $drupRouter = \Drupal::service('drup.router');
  $drupRouteName = $drupRouter->getName();

  /** @var \Drupal\drup\DrupPageEntity $drupPageEntity */
  $drupPageEntity = \Drupal::service('drup.page_entity');

  $viewId = $variables['view']->id();

  $variables['display_total_rows_tag'] = 'h2';
  $variables['display_total_rows_count'] = Drupal::translation()
    ->formatPlural(
      $variables['view']->total_rows,
      "<span>@count</span> result",
      "<span>@count</span> results"
    );

  if (empty($variables['rows'])) {
    $variables['display_total_rows_count'] = t('No results found');
    $variables['empty'] = t('Unfortunately your search did not return any results. Please broden your search and try again.');
    $variables['display_total_rows_tag'] = 'p';
    $variables['attributes']['class'][] = 'has-no-results';
  }

  switch ($viewId.'_'.$variables['view']->current_display) {
    case 'news_list_all':
    case 'publications_list_all':
      $variables['display_total_rows_count'] = \Drupal::translation()
        ->formatPlural(
          $variables['view']->total_rows,
          '@count result founded.',
          '@count results founded.'
        );
      if (empty($variables['rows'])) {
        $variables['empty'] = t('No result match your search.');
      }
      break;

    //        case 'examples':
    //           $variables['#attached']['library'][] = 'frontend/view-inspirations';
    //            $variables['title'] = $drupPageEntity->getBundle() === 'news' ? '' : t('Dans l\'actu');
    //            $variables['subtitle'] = $info['subtitle'];
    //            $variables['footer_link'] = [
    //                'cta' => $drupPageEntity->getBundle() === 'news' ? t('Retour aux actualités') : t('Toute l\'actu'),
    //                'uri' => $drupRouter->getUri('news'),
    //                'attributes' => new Attribute([
    //                    'class' => ['btn']
    //                ])
    //            ];
    //            break;
  }
}

/**
 * Preprocess Rows
 *
 * @param $variables
 */
function frontend_preprocess_views_view_unformatted(&$variables) {}

/**
 * Suggestions views
 *
 * @param  array  $suggestions
 * @param  array  $variables
 */
function frontend_theme_suggestions_views_view_unformatted_alter(array &$suggestions, array &$variables) {}

/**
 * Implements hook_preprocess_views_infinite_scroll_pager().
 *
 * @param $vars
 */
function frontend_preprocess_views_infinite_scroll_pager(&$vars) {
  $vars['btn_classes'] = 'btn';

  //if (isset($vars['parameters']['field_localisation_proximity'])) {
  //    $vars['btn_classes'] .= ' btn--link';
  //} else {
  $vars['btn_classes'] .= ' btn--primary';
  //}
}
