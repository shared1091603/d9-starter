<?php

use Drupal\drup_site\Utility\ContentUtility;

/**
 * @param  array  $suggestions
 * @param  array  $variables
 */
function frontend_theme_suggestions_node_alter(array &$suggestions, array $variables) {
  if (isset($variables['elements']['#node'])) {
    //        if (Annonce::is($variables['elements']['#node']->getType())) {
    //            $suggestions[] = 'node__annonce';
    //        }
  }
}

/**
 * @param  array  $variables
 */
function frontend_preprocess_node(array &$variables) {
  /** @var \Drupal\drup\Entity\Node $node */
  $node = $variables['elements']['#node'];
  $viewMode = $variables['elements']['#view_mode'];

  $nodeType = $node->getType();
  $entityField = $node->entityField();

  /** @var \Drupal\drup_router\DrupRouter $drupRouter */
  $drupRouter = \Drupal::service('drup.router');

  $variables['language'] = \Drupal::languageManager()->getCurrentLanguage()->getId();
  $variables['node_url'] = $node?->toUrl();

  $variables['fields'] = [];

  if ($viewMode === 'card') {
    $thumbnail = ContentUtility::getThumbnail($node);
    if (!$thumbnail instanceof \Drupal\drup\Entity\Media) {
      $thumbnail = ContentUtility::getPlaceholderThumbnail();
    }
    $thumbnailImageStyle = 'card_primary';

    $variables['fields']['thumbnail'] = $thumbnail->renderImage($thumbnailImageStyle);
    $variables['fields']['summary'] = ContentUtility::getSummary($node, 160);
    $variables['fields']['tags'] = [];
    $variables['fields']['cta'] = NULL;
  }

  elseif ($viewMode === 'full') {
    //    if ($nodeType === 'news') {
    //        $variables['back_link'] = [
    //            'title' => t('Retour aux actualités'),
    //            'url' => $drupRouter->getUrl('news')
    //        ];
    //    }
  }
}
