<?php

/**
 * @inheritdoc
 */
function frontend_preprocess_file_link(&$variables) {}

/**
 * @inheritdoc
 */
function frontend_preprocess_file_managed_file(&$variables) {
  //unset($variables['attributes']['id']);
  $variables['element']['label']['#attributes']['class'][] = 'btn';

  /**
   * @see https://www.drupal.org/docs/8/modules/webform/webform-cookbook/how-to-customize-the-text-for-a-file-upload-input
   */
  // Don't alter hidden file upload input.
  if (isset($variables['element']['upload']['#access']) && $variables['element']['upload']['#access'] === FALSE) {
    return;
  }
  // Create an unique id for the file upload input and label.
  $id = \Drupal\Component\Utility\Html::getUniqueId($variables['element']['upload']['#id'].'-target');
  // Create a label that is styled like an action button.
  $label = [
    '#type' => 'html_tag',
    '#tag' => 'label',
    '#value' => t('Choose a file'),
    //'#value' => $variables['element']['#title'],
    '#attributes' => [
      'for' => $id,
      'class' => 'btn btn--upload icon icon--upload',
    ],
  ];
  // Make sure the label is first.
  //$variables['element'] = ['label' => $label] + $variables['element'];
  $variables['element']['label'] = $label;
  // Set the custom ID for file upload input.
  $variables['element']['upload']['#attributes']['id'] = $id;
  // Hide the file upload.
  //$variables['element']['upload']['#attributes']['style'] = 'position: fixed; top: -1000px;';
  $variables['element']['upload']['#attributes']['class'][] = 'sr-only';

  $variables['elements']['remove_button']['#attributes']['class'][] = 'btn';
  $variables['elements']['remove_button']['#attributes']['class'][] = 'btn--simple';
}
