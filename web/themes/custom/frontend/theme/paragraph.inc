<?php

use Drupal\Component\Utility\Html;
use Drupal\drup\DrupPageEntity;
use Drupal\drup\Entity\EntityField;
use Drupal\drup_router\DrupRouter;
use Drupal\filter\Render\FilteredMarkup;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs_library\Entity\LibraryItem;

/**
 * Preprocess des contenus Paragraphs
 *
 * @param  array  $variables
 */
function frontend_preprocess_paragraph(array &$variables) {
  /** @var Paragraph $paragraph */
  $paragraph = $variables['paragraph'];

  $entityField = new EntityField($paragraph);
  $languageId = Drupal::languageManager()->getCurrentLanguage()->getId();

  /** @var DrupRouter $drupRouter */
  $drupRouter = Drupal::service('drup.router');

  /** @var DrupPageEntity $drupPageEntity */
  $drupPageEntity = Drupal::service('drup.page_entity');

  $variables['#attached']['library'][] = 'frontend/theme-paragraphs';
  $variables['attributes']['data-id'] = $paragraph->id();

  /* GLOBALS
     ========================================================================== */
  $hasRow = TRUE;

  $paragraphParent = $paragraph->getParentEntity();

  if ($paragraphParent instanceof LibraryItem) {
    $variables['attributes']['class'][] = 'from-library';
  }
  elseif ($paragraphParent instanceof Paragraph) {
    $variables['paragraph_parent'] = $paragraphParent;

    if ($paragraphParent->getType() === 'layout_row') {
      $variables['attributes']['class'][] = 'item-column';
      $hasRow = FALSE;
    }
  }

  //  if ($variables['media_layout'] = $entityField->getValue('orientation', 'value')) {
  //    $variables['attributes']['class'][] = 'l-'.Html::cleanCssIdentifier($variables['media_layout']);
  //  }

  //  if ($entityField->hasField('grid_options')) {
  //    $option = $entityField->getValue('grid_options', 'value') ?? 'default';
  //    $variables['attributes']['class'][] = 'l-'.$option;
  //  }

  // Themes
  if ($entityField->hasField('theme_background') && ($theme = $entityField->getValue('theme_background', 'value'))) {
    $variables['attributes']['class'][] = 'theme-'.Html::cleanCssIdentifier($theme);
    $variables['attributes']['class'][] = 'l-theme-background-full';
  }

  switch ($paragraph->getType()) {
    /* Text
       ========================================================================== */
    case 'text':
      //      if ($options = $entityField->getListTextLabels('text_options')) {
      //        foreach ($options as $option => $label) {
      //          $variables['attributes']['class'][] = 'l-'.Html::cleanCssIdentifier($option);
      //        }
      //      }
      break;

    /* Image or Video
       ========================================================================== */
    case 'media':
      if ($paragraphParent instanceof Paragraph) {
        // Set display of media to Image display/content_half : /admin/structure/media/manage/image/display/content_half
        $variables['content']['field_media'][0]['#view_mode'] = 'content_half';
      }
      break;

    /* HTML
   ========================================================================== */
    case 'html':
      if ($content = $entityField->getValue('code_html', 'value')) {
        $variables['content']['field_code_html'][0] = [
          '#markup' => FilteredMarkup::create($content),
        ];
      }
      break;

    /* PUSH
       ========================================================================== */
    case 'push':
      if ($hasRow) {
        $variables['attributes']['class'][] = 'row--big';
      }
      break;

    /* Testimony
       ========================================================================== */
    case 'testimony':
      if ($hasRow) {
        //        $variables['attributes']['class'][] = 'row--small';
      }
      break;

    /* GALLERY
       ========================================================================== */
    case 'gallery':
      if ($medias = $entityField->getReferencedEntities('medias')) {
        /** @var \Drupal\drup\Entity\Media $media */
        foreach ($medias as $index => $media) {
          $variables['images'][] = [
            'thumbnail' => $media->renderImage('gallery'),
            'modal_url' => $media->getReferencedFile()->getUrl('modal'),
          ];
        }
      }
      break;

    /* ACCORDIONS
   ========================================================================== */
    case
    'accordion':
      $output = [
        '#theme' => 'frontend_accordions',
        '#items' => [],
      ];

      $items = $entityField->getReferencedEntities('items');
      foreach ($items as $item) {
        $itemEntityField = new EntityField($item);

        $body = $itemEntityField->getProcessedText('body');
        $output['#items'][] = [
          'title' => $itemEntityField->getValue('title', 'value'),
          'body' => $body ? current($body) : NULL,
        ];
      }
      $variables['content'] = $output;
      break;

    /* ROW LAYOUT
    ========================================================================== */
    case 'layout_row';
      $variables['attributes']['class'][] = 'l-splitted';

      $subTypes = [];

      foreach ($entityField->getReferencedEntities('row_column') as $subParagraph) {
        $subTypes[] = $subParagraph->bundle();
        $variables['attributes']['class'][] = 'child-'.$subParagraph->bundle();
      }
      $variables['attributes']['class'][] = 'children-'.implode('-', $subTypes);
      $variables['attributes']['data-subtypes'] = implode('-', $subTypes);
      break;
  }

  /* final
     ========================================================================== */
  if ($hasRow) {
    $variables['attributes']['class'][] = 'row';
  }
}
