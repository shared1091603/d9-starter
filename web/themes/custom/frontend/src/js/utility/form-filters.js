(function ($, Drupal) {
  'use strict';

  /**
   * Formulaires des vues
   */
  Drupal.behaviors.frontendFormFilters = {
    context: undefined,
    settings: undefined,
    common: undefined,
    elements: {
      form: undefined,
    },

    config: {},

    /**
     * Paramètres nécessaires
     */
    prepare: function (context, settings) {
      this.common = Drupal.Frontend.Common;
      this.context = context;
      this.settings = settings;

      this.elements.form = $(context).is('.form--filters-primary') ? $(context) : $('.form--filters-primary', context);

      return this.elements.form.length > 0;
    },

    /**
     * A l'init
     *
     * @param context
     * @param settings
     */
    attach: function (context, settings) {
      if (this.prepare(context, settings)) {
        $.extend(this.elements, {
          submit: this.elements.form.find('.form-submit'),
          formWrapper: this.elements.form.children('.views-exposed-form--wrapper'),
          formTitle: this.elements.form.find('.form-title'),
        });

        if (this.elements.form.hasClass('views-exposed-form')) {
          // this.redirectHandler();
          this.displayFormValues();
          this.displayHandler();
          this.resultsListHandler();
        }
      }
    },

    /**
     * Change le select items_per_page selon le breakpoint de la page
     * Desktop = première option
     * Mobile = deuxième option
     */
    resultsListHandler: function () {
      let self = this;

      // if (this.elements.formItemResultsPerPage.length) {
      //   let options = this.elements.formItemResultsPerPage.find('option');
      //   let option = $(window).width() < 1000 ? options.eq(0) : options.eq(1);
      //
      //   if (option.attr('value') !== this.elements.formItemResultsPerPage.val()) {
      //     this.elements.formItemResultsPerPage.val(option.attr('value')).trigger('change');
      //   }
      // }
    },

    /**
     * Affiche le toggle du formulaire plié/déplié
     */
    displayHandler: function () {
      let self = this;

      // $(once('form-title-toggle', self.elements.formTitle)).each(function (i, button) {
      //   button = $(button);
      //   let form = button.parents('form').eq(0);
      //   let form_wrapper = form.find('.views-exposed-form--wrapper');
      //
      //   button.removeClass('sr-only')
      //     .detach().prependTo(form)
      //     .on('click', function (event) {
      //       event.preventDefault();
      //
      //       if ($(window).width() < 1000) {
      //         form.toggleClass('is-close');
      //         form_wrapper.slideToggle();
      //       }
      //     });
      //
      //   if ($(window).width() < 1000) {
      //     form.addClass('is-close');
      //     form_wrapper.hide();
      //
      //     // Scroll auto si déjà filtré
      //     if (window.location.search.length > 1) {
      //       setTimeout(function () {
      //         window.scroll({
      //           top: form.offset().top - 74 - 30,
      //         });
      //       }, 100);
      //     }
      //   }
      //
      //   $(window).on('resize.filters', function (event) {
      //     if ($(this).width() < 1000) {
      //       form.addClass('is-close');
      //       form_wrapper.hide();
      //     } else {
      //       form.removeClass('is-close');
      //       form_wrapper.show();
      //     }
      //   });
      // });
    },

    /**
     * Met à jour le nombre de filtres actifs
     */
    updateValuesCounter: function (counter) {
      this.elements.formTitle.find('.counter').text(counter).toggle(counter > 0);
    },

    /**
     * Affiche les valeurs des filtres sous forme de tags
     * Au click sur un tag, le champ correspondant est reset
     */
    displayFormValues: function () {
      let self = this;

      // Liste des valeurs du formulaire
      let formValues = self.elements.form.serializeArray();
      let tags = [];

      let itemNameExcluded = ['lat', 'lng', 'items_per_page'];

      // Ajout des tags correspondant à chaque valeur de filtre sous forme "Label : Valeur"
      $.each(formValues, function (i, item) {
        if (item.value.trim().length && item.value !== 'All' && $.inArray(item.name, itemNameExcluded) === -1) {
          //let input = self.elements.form.find('[name="' + item.name + '"][value="' + item.value + '"]');
          let input = self.elements.form.find('[name="' + item.name + '"]');
          let isRadioOrCheckbox = input.attr('type') === 'checkbox' || input.attr('type') === 'radio';

          let label = isRadioOrCheckbox ? input.siblings('label').eq(0) : self.elements.form.find('label[for="' + input.attr('id') + '"]');
          let displayedLabel = label.text(); // nom du champ à afficher dans le tag

          // Ajout de la valeur si un label est présent
          let displayedValue = null;
          if (input.length && label.length && input.attr('type') !== 'hidden') {
            // Select : Valeur de l'option
            if (input.is('select')) {
              displayedValue = input.find('option[value="' + item.value + '"]').text();
            } else {
              // Checkbox ou radio
              if (isRadioOrCheckbox) {
                let formWrapper = input.closest('.form-wrapper');

                // Le label est la légende du form group si existant
                if (formWrapper.length && formWrapper.find('.fieldset-legend')) {
                  displayedLabel = formWrapper.find('.fieldset-legend').text();
                  displayedValue = label.text();
                } else {
                  displayedLabel = self.elements.form.find('label[for="' + input.attr('id') + '"]').text();
                  displayedValue = label.text();
                }
              }
              // Sinon tout sauf hidden
              else {
                displayedValue = item.value;
              }
            }
          }

          if (displayedValue !== null) {
            // Si le label et la valeur sont identique, on affiche seulement la valeur
            if (displayedValue === displayedLabel) {
              displayedLabel = null;
            } else {
              displayedLabel = displayedLabel.trim();
            }

            tags.push({
              input: input,
              name: item.name,
              label: displayedLabel,
              display_value: displayedValue,
              input_value: item.value,
            });
          }
        }
      });

      if (tags.length) {
        // Container des futurs tags
        let formTags = self.elements.form.find('.form-tags');
        // if (formTags.length === 0) {
        //   formTags = $('<div>', {
        //     'class': 'form-tags tags',
        //   }).prependTo(self.elements.form.find('.views-exposed-form--wrapper'));
        // }

        // Ajout des buttons
        $.each(tags, function (i, item) {
          let tag = $('<button>', {
            'type': 'button',
            // 'title': Drupal.t('Supprimer la valeur ' + item.display_value + ' pour le filtre ' + item.label),
            // 'title': Drupal.t('Remove @value for the @filter filter', { '@value': item.display_value, '@filter': item.label }),
            'class': 'tag tag--filter',
            // 'html': item.label !== null ? item.label + ' : ' + item.display_value : item.display_value
            'html': item.display_value,
          }).appendTo(formTags);

          // Suppression de la valeur au clic sur le filtre
          tag.on('click', function () {
            // Select
            if (item.input.is('select')) {
              // Spécial multiple
              if (item.input.is('[multiple]') === false) {
                item.input.prop('selectedIndex', 0);
                // item.input.val('');
              } else {
                item.input.find('option[value="' + item.input_value + '"]').prop('selected', false);
              }
            }
            // Checkbox
            else if (item.input.attr('type') === 'checkbox' || item.input.attr('type') === 'radio') {
              item.input.prop('checked', false);
            }
            // Autres
            else {
              item.input.val('');
              // Si l'input est configuré avec daterangerpicker, on trigger le bouton "Annuler"
              // if (self.config.datePicker !== undefined && self.config.datePicker.instance !== undefined && item.input.is(self.config.datePicker.instance)) {
              //     self.config.datePicker.instance.data('daterangepicker').clickCancel();
              // }

              // Si l'input est de type geolocation, on unset aussi lat/lng
              // if (item.name === 'ville') {
              //     self.elements.form.find('input.geolocation-input-latitude, input.geolocation-input-longitude').val('');
              // }
            }

            // Si le filtre est déjà dans l'url (pour un formulaire ajax submit), on recharge la page pour forcer sa suppression des filtres
            self.submitForm();
            // if (decodeURI(window.location.search).indexOf(item.name) >= 0) {
            //   self.submitForm();
            // }
            // // sinon classique chargement ajax
            // else {
            //   item.input.trigger('change');
            // }
          });
        });

        // Tag "Tout supprimer" si au moins 1 boutons
        if (tags.length > 0) {
          $('<button>', {
            'type': 'button',
            'title': Drupal.t('Supprimer toutes les valeurs de filtrage'),
            'class': 'tag tag--filter',
            'html': Drupal.t('Tout désélectionner')
          })
            .appendTo(formTags)
            .on('click', function () {
              self.elements.form.find('input, textarea, select')
                .val('')
                .prop('checked', false)
                .prop('selected', false);

              self.elements.submit.trigger('click');
            });
        }
        self.elements.form.addClass('has-filter-tags');
      } else {
        self.elements.form.removeClass('has-filter-tags');
      }

      this.updateValuesCounter(tags.length);
    },

    /**
     *
     */
    submitForm: function () {
      this.elements.form.find('.form-submit').trigger('click');
    },
  };

}(jQuery, Drupal));
