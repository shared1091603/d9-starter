(function ($, Drupal) {
  Drupal.behaviors.frontendAccordions = {
    attach: function (context, settings) {
      $(once('accordion', '.accordion')).each(function (i, accordion) {
        accordion = $(accordion);

        accordion.find('.item-content').hide();

        accordion.find('.item-title').on('click', function (event) {
          let toggle = $(event.currentTarget);
          let container = toggle.closest('.accordion-item');
          let content = container.find('.item-content');

          container.toggleClass('is-opened')
            .siblings()
            .removeClass('is-opened')
            .find('.item-content')
            .slideUp('fast');

          content.slideToggle('fast');
        });
      });
    },
  };
})(jQuery, Drupal);
