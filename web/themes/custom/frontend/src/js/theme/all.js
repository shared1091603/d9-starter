(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.themeAll = {
    common: undefined,
    context: undefined,
    settings: {},

    elements: {},

    configs: {},

    isAjax: false,

    /**
     *
     * @param context
     * @param settings
     * @returns {boolean}
     */
    prepare: function (context, settings) {
      this.common = Drupal.Frontend.Common;
      this.context = context;
      this.settings = settings;

      this.isAjax = this.context.body === undefined;

      return this.isAjax === false;
    },

    /**
     * A l'init
     *
     * @param context
     * @param settings
     */
    attach: function (context, settings) {
      if (this.prepare(context, settings)) {
        $.extend(this.elements, {
          body: this.common.elements.body,
          externalLinks: $('a[href^="http"]', this.context),
          blockShare: $('.nav--social-share', this.context),
          navMain: $('#nav-main', this.context),
          toggleMenuToggle: $('#togglemenu-toggle', this.context),
          openInNewWindowLinks: this.common.elements.page.find('a[href$=".pdf"]'),
          ckeditorButtons: this.common.elements.content.find('.item-body .btn'),
          accordions: $('.accordion', this.context),
        });

        this.stickyHandler();
        this.navMainHandler();

        this.ckeditorButtonsHandler();

        this.openInNewWindowHandler();
        this.anchorLinksHandler();
      }

      // Uniquement après event Ajax
      if (this.isAjax === true) {
      }
    },

    /**
     *
     */
    stickyHandler: function () {
      let self = this;

      /* Header
         ========================================================================== */
      let height = Math.floor(self.common.elements.mainHeader.height());
      self.common.elements.pageOffset.css('height', height);

      self.common.libraries.headerFixer = self.common.elements.mainHeader.fixer({
        start: 0,
        onFixed: function () {
          let height = self.common.elements.mainHeader.height();
          self.common.elements.pageOffset.css('height', height);
          self.common.elements.body.addClass('is-main-header-reduced');
        },
        onReset: function () {
          self.common.elements.body.removeClass('is-main-header-reduced');
        },
        onBottom: function () {
          self.common.elements.body.removeClass('is-main-header-reduced');
        },
      });
    },

    /**
     * Gestionnaire de la navigation principale
     */
    navMainHandler: function () {
      let self = this;

      // Nav Main
      if (self.elements.navMain.length) {
        self.common.libraries.toggleMenu = new $.ToggleMenu();

        // @link https://github.com/kevinbaubet/togglemenu/blob/master/docs/hover.md
        self.common.libraries.toggleMenu.setOptions('hover', {
          elements: {
            menu: self.elements.navMain,
          },
          interval: 300,
        });

        // Tablet / Mobile
        // @link https://github.com/kevinbaubet/togglemenu/blob/master/docs/push.md
        self.common.libraries.toggleMenu.setOptions('push', {
          elements: {
            toggle: self.elements.toggleMenuToggle,
            content: {
              close: Drupal.t('Close'),
              menu: self.elements.navMain.children(),
            },
          },
          backLink: true,
          layout: 'panel',
          onToggle: function () {
            let top = self.common.elements.mainHeader.position().top + self.common.elements.mainHeader.height();
            this.elements.wrapper.css('top', top);
          },
          onAddContent: function () {
            if (this.type === 'menu') {
              let menu = this.toggleMenuPush.elements.itemContent(this.content);

              // $('<li>', {
              //   'class': 'nav-item is-homepage',
              //   html: '<a href="' + Drupal.url('') + '">' +
              //     //'<span class="icon icon--home"></span>' +
              //     '<span class="item-title">' + Drupal.t('Home') + '</span>' +
              //     '</a>',
              // }).prependTo(menu);
            }
          },
        });

        self.common.libraries.toggleMenu.addMenu(self.common.isUnderResponsiveMenuBreakpoint() ? 'push' : 'hover');
        self.common.libraries.deviceDetect.onResize(function () {
          self.common.libraries.toggleMenu.toggleMenu(self.common.isUnderResponsiveMenuBreakpoint() ? 'push' : 'hover');
        });
      }
    },

    /**
     * Ajout un <span> dans les boutons générés par CKEditor
     */
    ckeditorButtonsHandler: function () {
      $.each(this.elements.ckeditorButtons, function (i, item) {
        item = $(item);

        if (item.children('span').length === 0) {
          item.html($('<span>', {
            html: item.html(),
          }));
        }
      });
    },
    /**
     * Gestionnaire pour l'ouverture de lien dans un nouvel onglet
     */
    openInNewWindowHandler: function () {
      if (this.elements.openInNewWindowLinks.length) {
        this.elements.openInNewWindowLinks.on('click', function (event) {
          event.preventDefault();
          window.open($(event.currentTarget).attr('href'));
        });
      }
    },

    /**
     * Gestionnaire des ancres
     */
    anchorLinksHandler: function () {
      let self = this;

      self.common.elements.page.on('click', 'a[href*="#"]:not([href="#"])', function (event) {
        if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
          let target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');

          if (target.length) {
            event.preventDefault();

            self.common.scrollTop((target.offset().top - self.common.menuScrollOffset));
          }
        }
      });
    },
  };

}(jQuery, Drupal));
