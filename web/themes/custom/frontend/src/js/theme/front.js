(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.themeFront = {
    common: undefined,
    context: undefined,

    elements: {},
    settings: {},

    isAjax: false,

    /**
     * Paramètres nécessaires
     */
    prepare: function (context, settings) {
      this.common = Drupal.Frontend.Common;
      this.context = context;
      this.settings = settings;

      this.elements.node = $('.route--entity-node-canonical', context);
      this.isAjax = this.context.body === undefined;
      return true;
    },

    /**
     * A l'init
     *
     * @param context
     * @param settings
     */
    attach: function (context, settings) {
      if (this.prepare(context, settings)) {
        $.extend(this.elements, {});
      }
    },
  };
}(jQuery, Drupal));
