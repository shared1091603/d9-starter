(function ($, Drupal, drupalSettings) {
  'use strict';

  /** * JS orienté objet, chargé par Drupal au chargement du DOM + à chaque event Ajax */
  Drupal.behaviors.CHANGE = {
    /**
     * GENERIQUE
     */

    // Intégration de la librairies d'utils dans theme.js
    common: undefined,
    // La portion du DOM chargée
    context: undefined,
    // drupalSettings
    settings: undefined,
    // Si la méthode attach() a été appelée à l'initialisation du DOM ou après un event ajax
    isAjax: false,

    /**
     * SPECIFIQUE
     */
    // Identifier les principaux éléments HTML utilisés dans le plugin
    elements: {
      view: undefined,
      form: undefined,
    },
    // Et les configs/variables
    configs: {
      breakpoint: 1024,
    },
    // Etats, amenés à être modifiés dans les méthodes de la classe
    states: {
      formIsOpened: false
    },

    /**
     * Paramètres nécessaires
     * Populate les variables de la classe
     * Renvoie l'autorisation du plugin à être utilisé (tous les Drupal.attach du projet sont appelés sans distinction)
     */
    prepare: function (context, settings) {
      this.common = Drupal.Frontend.Common;
      this.context = context;
      this.settings = settings;

      if ($(this.context).is('.block--store-locators-list-filtered')) {
        this.elements.view = $(this.context);
        this.isAjax = true;
      } else {
        this.elements.view = $('.block--store-locators-list-filtered', this.context);
        this.isAjax = false;
      }

      return this.elements.view.length > 0;
    },

    /**
     * A l'init
     *
     * @param context
     * @param settings
     */
    attach: function (context, settings) {
      if (this.prepare(context, settings)) {
        // Ajout des éléments HTML enfant du container principal
        $.extend(this.elements, {
          // au miximum contextualiser le chargement jquery avec le container principal
          form: $('.views-exposed-form', this.elements.view),
          // $('.block--whatever', self.common.elements.contentBody) idem que : self.common.elements.contentBody.find('.block--whatever')
          footer: $('.block--whatever', this.common.elements.contentBody)
        });

        // Possible de faire appel à des méthodes seulement si c'est du premier chargement ou de l'ajax
        // Mais généralement pas besoin d'être utilisé
        if (this.isAjax === false) {
          this.myFirstHandler();
          this.mySecondHandler();
        } else {
          this.mySecondHandler();
        }
      }
    },

    /**
     *
     */
    myFirstHandler: function () {
      // Utiliser un pointer différent de this pour mieux identifier les this dans les events locaux du this qui représente la classe actuelle
      let self = this;

      if (self.elements.form.length === 0) {
        return;
      }

      // Librairies once() pour appliquer une seule fois un evenement à un élément jquery
      $(once('my-namespace', self.elements.form.find('select[data-selector="*"]'))).on('change', function (event) {
        let select = $(event.currentTarget);

        // ...
      });
    },

    /**
     *
     */
    mySecondHandler: function () {
      let self = this;

      // fonction locale
      let onResize = function () {
        self.states.formIsOpened = (self.configs.breakpoint > 2000);
        // ...
      };

      /**
       * Prefixer les events avec un namespace
       * @link https://api.jquery.com/event.namespace/
       */
      $(window).on('mynamespace.resize', function (event) {
        onResize();
      });
      onResize();
    },


  };

}(jQuery, Drupal, drupalSettings));
