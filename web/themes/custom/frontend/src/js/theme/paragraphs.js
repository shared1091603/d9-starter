(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.themeParagraphs = {
    common: undefined,
    context: undefined,

    elements: {
      wrapper: undefined,
    },
    settings: {},

    isAjax: false,

    /**
     * Paramètres nécessaires
     */
    prepare: function (context, settings) {
      this.common = Drupal.Frontend.Common;
      this.context = context;
      this.settings = settings;

      this.elements.wrapper = $('.item-paragraphs', context);
      this.isAjax = this.context.body === undefined;
      return true;
    },

    /**
     * A l'init
     *
     * @param context
     * @param settings
     */
    attach: function (context, settings) {
      if (this.prepare(context, settings)) {
        $.extend(this.elements, {
          sliderGallery: $('.slider--gallery')
        });

        this.sliderHandler();
      }
    },

    /**
     *
     */
    sliderHandler: function () {
      let self = this;

      if (this.elements.sliderGallery.length) {
        $(once('slider', this.elements.sliderGallery)).each(function (i, item) {
          let slider = self.common.initDefaultSwiper($(item), {
            // autoHeight: true,
            pagination: {
              enabled: true,
            },
            navigation: {
              enabled: false,
            },
            breakpoints: {
              1000: {
                pagination: {
                  enabled: false,
                },
                navigation: {
                  enabled: true,
                },
              },
            },
          });
        });
      }
    },
  };
}(jQuery, Drupal));
