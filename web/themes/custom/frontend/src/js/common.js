(function ($, Drupal, drupalSettings) {
  'use strict';

  Drupal.Frontend = {};

  // Fancybox default
  $.extend(true, $.fancybox.defaults, {
    lang: 'fr',
    i18n: {
      fr: {
        CLOSE: 'Fermer la fenêtre',
        NEXT: 'Suivante',
        PREV: 'Pr&eacute;c&eacute;dente',
        ERROR: 'Le contenu demand&eacute; ne peut pas &ecirc;tre charg&eacute;.<br />Veuillez r&eacute;essayer plus tard.',
        PLAY_START: 'D&eacute;marrer',
        PLAY_STOP: 'Pause',
        FULL_SCREEN: 'Plein &eacute;cran',
        THUMBS: 'Vignettes',
        DOWNLOAD: 'Télécharger',
        SHARE: 'Partager',
        ZOOM: 'Zoomer',
      },
    },
  });

  /**
   * Constructeur
   */
  let FrontendLibraries = function () {
    // Éléments
    this.elements = {
      htmlBody: $('html, body'),
      body: $('body'),
      page: $('#page'),
      mainHeader: $('#page-header'),
      pageOffset: $('#page-offset'),
      content: $('#content'),
    };

    //
    this.libraries = {
      deviceDetect: undefined,
      spinner: undefined,
      headerFixer: undefined,
      toggleMenu: undefined,
    };

    // Variables
    this.menuHeight = this.getCssValue('--header--height');
    this.menuScrollOffset = this.menuHeight + 60;
    this.responsiveMenuBreakpoint = this.getCssValue('--frontend--breakpoint--responsive-menu');
    this.breakpoints = this.getCssValues('--frontend-breakpoints');

    this.userIsLoggedIn = this.elements.body.hasClass('is-logged-in');
    this.pathToTheme = drupalSettings.pathToTheme || '';
    this.currentLanguageId = drupalSettings.path.currentLanguage || 'fr';

    return this;
  };

  /**
   * Méthodes
   */
  FrontendLibraries.prototype = {
    buildSliderPrevArrow: function () {
      return $('<button type="button" class="swiper-button-prev"><span class="sr-only">' + Drupal.t('Previous') + '</span><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20"><path fill-rule="evenodd" d="M7.852 16.426c.4.447 1.04.49 1.46.146.421-.345.49-1.04.146-1.46L6.02 10.94h9.745a1.035 1.035 0 1 0 0-2.07H6.02l3.438-4.17c.344-.42.267-1.107-.145-1.46-.425-.366-1.117-.276-1.46.145L3.023 9.248c-.314.442-.287.888 0 1.315l4.828 5.863Z" clip-rule="evenodd"/></svg></button>');
    },
    buildSliderNextArrow: function () {
      return $('<button type="button" class="swiper-button-next"><span class="sr-only">' + Drupal.t('Next') + '</span><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20"><path fill-rule="evenodd" d="M11.747 3.574c-.4-.447-1.04-.49-1.46-.146-.42.345-.49 1.04-.146 1.46L13.58 9.06H3.835a1.035 1.035 0 1 0 0 2.07h9.745l-3.438 4.17c-.344.42-.266 1.107.146 1.46.425.366 1.116.276 1.46-.145l4.829-5.863c.314-.441.287-.887 0-1.314l-4.829-5.863Z" clip-rule="evenodd"/></svg></button>');
    },
    /**
     *
     * @param $sliderWrapper
     * @param options
     * @returns {*}
     */
    initDefaultSwiper: function ($sliderWrapper, options) {
      let self = this;

      let swiperOptions = $.extend(true, {
        navigation: {
          enabled: true,
          prevEl: '.swiper-button-prev',
          nextEl: '.swiper-button-next',
        },
        pagination: {
          enabled: false,
          clickable: true,
          el: '.swiper-pagination',
        },
        slidesPerView: 'auto',
        spaceBetween: self.getCssValue('--frontend--gutter'),
        on: {
          resize: function (swiper) {
            let enabled = swiper.enabled;

            if (enabled) {
              $(swiper.el).addClass('is-enabled').removeClass('is-disabled');
            } else {
              $(swiper.el).addClass('is-disabled').removeClass('is-enabled');
            }
          },
          init: function (swiper) {
            let enabled = swiper.enabled;

            if (enabled) {
              $(swiper.el).addClass('is-enabled').removeClass('is-disabled');
            } else {
              $(swiper.el).addClass('is-disabled').removeClass('is-enabled');
            }
          },
        },
      }, options);

      // Build instance
      let sliderSelector = $sliderWrapper[0];
      if ($sliderWrapper.attr('data-slider-id').length) {
        sliderSelector = '[data-slider-id="' + $sliderWrapper.attr('data-slider-id') + '"]';
      }

      let swiperInstance = new Swiper(sliderSelector, swiperOptions);
      return swiperInstance;
    },

    /**
     * Retrieves the computed CSS value for the specified name.
     *
     * @param {string} name - The name of the CSS property.
     * @returns {string} - The computed CSS value for the specified name.
     */
    getCssValue: function (name) {
      return getComputedStyle(document.documentElement).getPropertyValue(name).replace('px','');
    },

    /**
     * Returns an array containing the CSS values for the specified property.
     * The CSS values are extracted from the CSS value string returned by the `getCssValue` function.
     *
     * @param {string} name - The name of the CSS property.
     * @returns {Array} - An array containing the CSS values as key-value pairs.
     *                    The keys represent the property names and the values represent the numerical values of the properties.
     */
    getCssValues: function (name) {
      let output = [];

      let items = this.getCssValue(name)
        .replaceAll('(', '')
        .replaceAll(')', '')
        .replaceAll('"', '')
        .replaceAll('px', '')
        .split(', ');
      items.forEach((item) => {
        let values = item.trim().split(': ');
        output[values[0]] = parseInt(values[1]);
      });

      return output;
    },

    /**
     * Gestionnaire de la détection des périphériques
     */
    deviceDetectHandler: function () {
      let self = this;

      self.libraries.deviceDetect = new $.DeviceDetect();
      self.libraries.deviceDetect.getDevices();

      if (self.libraries.deviceDetect.is('desktop') && self.detectMobileIOS() === true) {
        self.libraries.deviceDetect.setType('tablet').setDevices();
      }
    },
    isUnderScreenSmallWidth() {
      return this.libraries.deviceDetect.getWindowWidth() <= this.libraries.deviceDetect.settings.maxWidth.tablet;
    },
    isUnderResponsiveMenuBreakpoint() {
      return this.libraries.deviceDetect.getWindowWidth() <= this.responsiveMenuBreakpoint;
    },

    /**
     * @see https://informatux.com/news/article/77/comment-differencier-un-ipad-d-un-mac-avec-le-user-agent-depuis-la-version-13-d-ios-
     * @returns {boolean}
     */
    detectMobileIOS: function () {
      let isMobileIOS = false;
      let isMobileMac = RegExp(/webOS|iPhone|iPod|iPad/i).test(navigator.userAgent);
      if (isMobileMac) {
        isMobileIOS = true;
      } else {
        const isMac = RegExp(/Macintosh/i).test(navigator.userAgent);
        if (isMac) {
          if (navigator.maxTouchPoints && navigator.maxTouchPoints > 2) {
            isMobileIOS = true;
          }
        } else {
          isMobileIOS = false;
        }
      }
      return isMobileIOS;
    },

    /**
     * Gestionnaire du loader sur les requêtes ajax
     */
    spinnerHandler: function () {
      let options = {
        auto: true,
        autoPathsExceptions: [
          'spinner=0',
          'googletagmanager.com',
        ],
      };

      this.libraries.spinner = this.elements.body.spinner(options);
    },

    /**
     * Scroll top
     *
     * @param offset
     * @param speed
     */
    scrollTop: function (offset, speed) {
      speed = speed || 500;

      this.elements.htmlBody.animate({
        scrollTop: offset,
      }, speed);

      return this;
    },
  };

  Drupal.behaviors.frontend = {
    attach: function (context) {
      if (context.body !== undefined) {
        // Instanciate the Common library to be used is other /theme/*.js files
        Drupal.Frontend.Common = new FrontendLibraries();
        Drupal.Frontend.Common.deviceDetectHandler();
        Drupal.Frontend.Common.spinnerHandler();
      }
    },
  };

}(jQuery, Drupal, drupalSettings));
