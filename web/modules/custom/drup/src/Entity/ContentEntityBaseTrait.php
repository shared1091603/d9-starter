<?php

namespace Drupal\drup\Entity;

trait ContentEntityBaseTrait {

  /**
   * @var \Drupal\drup\Entity\EntityField
   */
  protected $entityField;

  /**
   * @return \Drupal\drup\Entity\EntityField
   */
  public function entityField() {
    if (!$this->entityField instanceof EntityField) {
      $this->entityField = new EntityField($this);
    }

    return $this->entityField;
  }

}
