<?php

namespace Drupal\drup\Entity;

use Drupal\Core\Menu\MenuTreeParameters;

/**
 *  Menu
 */
class Menu {

  /**
   * @var \Drupal\Core\Menu\MenuTreeParameters
   */
  protected MenuTreeParameters $menuTreeParameters;

  /**
   * @var \Drupal\Core\Menu\MenuLinkTreeInterface
   */
  protected \Drupal\Core\Menu\MenuLinkTreeInterface $menuTreeService;

  /**
   * @var string
   */
  protected string $menuName;

  /**
   * @var array
   */
  protected array $menuLinkTreeElements = [];

  /**
   *
   */
  public function __construct() {
    $this->menuTreeParameters = new MenuTreeParameters();
    $this->menuTreeService = \Drupal::menuTree();
  }

  /**
   * @return static
   */
  public static function create() {
    return new static();
  }

  /**
   * @param  \Drupal\drup\Entity\MenuItem  $menuItem
   *
   * @return static
   */
  public static function createFromMenuItem(MenuItem $menuItem) {
    $instance = new static();
    $instance->setMenuName($menuItem->entity()->getMenuName());
    $instance->menuTreeParameters->setRoot($menuItem->entity()->getPluginId());

    return $instance;
  }

  /**
   * @param  string  $menuName
   *
   * @return $this
   */
  public function setMenuName(string $menuName) {
    $this->menuName = $menuName;

    return $this;
  }

  /**
   * @param  string|NULL  $root
   * @param  int  $maxDepth
   *
   * @return $this
   */
  public function applyParameters(string $root = NULL, int $maxDepth = 1) {
    $this->menuTreeParameters->onlyEnabledLinks();
    $this->menuTreeParameters->excludeRoot();

    if ($root) {
      $this->menuTreeParameters->setRoot($root);
    }
    if ($maxDepth > 0) {
      $this->menuTreeParameters->setMaxDepth($maxDepth);
    }

    return $this;
  }

  /**
   * @return array
   */
  public function getMenuLinkTreeElements(): array {
    return $this->menuLinkTreeElements;
  }

  /**
   * @return \Drupal\drup\Entity\MenuItem[]
   */
  public function getItems(): array {
    $items = [];

    if (empty($this->menuLinkTreeElements)) {
      $this->applyMenuTree();
    }

    if (!empty($this->menuLinkTreeElements)) {
      $items = $this->addItems($this->menuLinkTreeElements);
    }

    return $items;
  }

  /**
   * @param  array  $manipulators
   *
   * @return $this
   */
  public function applyMenuTree(array $manipulators = []) {
    $manipulators = !empty($manipulators) ? $manipulators : [
      ['callable' => 'menu.default_tree_manipulators:checkNodeAccess'],
      ['callable' => 'menu.default_tree_manipulators:checkAccess'],
      ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
    ];
    $menuTreeLoad = $this->menuTreeService->load($this->menuName, $this->menuTreeParameters);
    $this->menuLinkTreeElements = $this->menuTreeService->transform($menuTreeLoad, $manipulators);

    return $this;
  }

  /**
   * @param  array  $menuLinkTreeElements
   *
   * @return \Drupal\drup\Entity\MenuItem[]
   */
  protected function addItems(array $menuLinkTreeElements) {
    $output = [];

    if (!empty($menuLinkTreeElements)) {
      foreach ($menuLinkTreeElements as $menuLinkTreeElement) {
        $item = MenuItem::createFromPluginId($menuLinkTreeElement->link->getPluginId());
        if (!empty($menuLinkTreeElement->subtree)) {
          $item->below = $this->addItems($menuLinkTreeElement->subtree);
        }

        $output[] = $item;
      }
    }

    return $output;
  }

}
