<?php

namespace Drupal\drup\Entity;

/**
 * Class Term
 *
 * @package Drupal\drup\Entity
 */
class Term extends \Drupal\taxonomy\Entity\Term {

  use ContentEntityBaseTrait;
  use ContentEntityBaseTranslatableTrait;

  /**
   * @return Term|null
   */
  public function getParent() {
    $ancestors = \Drupal::service('entity_type.manager')->getStorage('taxonomy_term')->loadParents($this->id());

    if (!empty($ancestors)) {
      return current($ancestors);
    }

    return NULL;
  }

}
