<?php

namespace Drupal\drup\Entity;

/**
 * Class Node
 *
 * @package Drupal\drup\Entity
 */
class Node extends \Drupal\node\Entity\Node {

  use ContentEntityBaseTrait;
  use ContentEntityBaseTranslatableTrait;

  /**
   * @return string
   */
  public function getName() {
    return $this->getTitle();
  }

}
