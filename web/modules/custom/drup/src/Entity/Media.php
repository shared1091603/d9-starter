<?php

namespace Drupal\drup\Entity;

/**
 * Class Media
 *
 * @package Drupal\drup\Entity\Media
 */
class Media extends \Drupal\media\Entity\Media {

  use ContentEntityBaseTrait;
  use ContentEntityBaseTranslatableTrait;

  /**
   * Rendu d'une image représentant le média
   *
   * @param  null  $style
   * @param  array  $attributes
   *
   * @return array|null
   */
  public function renderImage($style = NULL, array $attributes = []) {
    $file = $this->getReferencedFile() ?? $this->getThumbnailFile();

    if ($file) {
      // ALT
      if (!isset($attributes['alt'])) {
        $media_source = $this->getSource();
        $plugin_definition = $media_source->getPluginDefinition();

        if (!empty($plugin_definition['thumbnail_alt_metadata_attribute'])) {
          $attributes['alt'] = $media_source->getMetadata($this, $plugin_definition['thumbnail_alt_metadata_attribute']);
        }
      }

      return $file->renderImage($style, $attributes);
    }

    return NULL;
  }

  /**
   *
   * Fichier (entité File) principal lié au Media
   *
   * @return \Drupal\drup\Entity\File|null
   */
  public function getReferencedFile() {
    $source = $this->getSource();

    if ($fid = $source->getSourceFieldValue($this)) {
      return File::createFromFid($fid);
    }

    return NULL;
  }

  /**
   * Miniature du Média générée par Drupal
   *
   * @return \Drupal\drup\Entity\File|null
   */
  public function getThumbnailFile() {
    if ($this->hasField('thumbnail') && !$this->get('thumbnail')->isEmpty()) {
      return File::createFromFid($this->get('thumbnail')->target_id);
    }

    return NULL;
  }

}
