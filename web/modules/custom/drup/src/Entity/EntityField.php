<?php

namespace Drupal\drup\Entity;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\field\Entity\FieldConfig;

/**
 *
 * Class entityField
 *
 * @package Drupal\drup\Entity
 */
class EntityField {

  /**
   * @var FieldableEntityInterface
   */
  protected FieldableEntityInterface $entity;

  /**
   * entityField constructor.
   *
   * @param  FieldableEntityInterface  $entity
   */
  public function __construct(FieldableEntityInterface $entity) {
    $this->entity = $entity;
  }

  /**
   * @param  FieldableEntityInterface  $entity
   *
   * @return static
   */
  public static function create(FieldableEntityInterface $entity) {
    return new static($entity);
  }

  /**
   * @param  string  $field
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   */
  public function getReferencedEntity(string $field) {
    /** @var \Drupal\Core\Field\EntityReferenceFieldItemList $fields */
    if (($fields = $this->get($field)) && $entities = $fields->referencedEntities()) {
      return current($entities);
    }

    return NULL;
  }

  /**
   * @param  string  $field
   *
   * @return \Drupal\Core\Field\FieldItemListInterface|false
   */
  public function get(string $field) {
    if ($this->hasField($field) && ($data = $this->entity()->get(self::format($field))) && !$data->isEmpty()) {
      return $data;
    }

    return FALSE;
  }

  /**
   * @param  string  $field
   *
   * @return bool
   */
  public function hasField(string $field) {
    return $this->entity()->hasField(self::format($field));
  }

  /**
   * @return \Drupal\Core\Entity\FieldableEntityInterface
   */
  public function entity() {
    return $this->entity;
  }

  /**
   * @param  string  $field
   *
   * @return string
   */
  public static function format(string $field) {
    //return in_array($field, ['title']) ? $field : 'field_' . $field;
    return in_array($field, []) ? $field : 'field_'.$field;
  }

  /**
   * @param  string  $field
   *
   * @return bool
   */
  public function hasValue(string $field) {
    return !$this->entity()->get(self::format($field))?->isEmpty();
  }

  /**
   * @param  string  $field
   *
   * @return array|\Drupal\Core\Entity\EntityInterface[]
   */
  public function getReferencedEntities(string $field) {
    /** @var \Drupal\Core\Field\EntityReferenceFieldItemList $fields */
    if ($fields = $this->get($field)) {
      return $fields->referencedEntities();
    }

    return [];
  }

  /**
   * @param  string  $field
   * @param  string  $key
   *
   * @return array|null
   */
  public function getProcessedText(string $field, string $key = 'value') {
    $values = [];

    if ($fieldEntity = $this->get($field)) {
      foreach ($fieldEntity->getIterator() as $item) {
        $data = $item->getValue();

        if (!empty($data) && isset($data[$key])) {
          $values[] = [
            '#type' => 'processed_text',
            '#text' => $data[$key],
            '#format' => $data['format'],
          ];
        }
      }

      return $values;
    }

    return NULL;
  }

  /**
   * @param  string  $field
   * @param  null|string  $key
   *
   * @return string|array|null
   */
  public function getValue(string $field, string $key = NULL) {
    if (($fields = $this->get($field)) && ($fistField = $fields->first()) && ($data = $fistField->getValue())) {
      if (is_string($key)) {
        if (isset($data[$key])) {
          return $data[$key];
        }
      }
      else {
        return $data;
      }
    }

    return NULL;
  }

  /**
   * @param  string  $field
   *
   * @return array
   */
  public function getListTextLabels(string $field) {
    if ($values = $this->getValues($field, 'value')) {
      if ($settingsFunction = $this->get($field)->getSetting('allowed_values_function')) {
        $allowedValues = $settingsFunction();
      }
      else {
        $allowedValues = $this->get($field)->getSetting('allowed_values');
      }

      return array_intersect_key($allowedValues, array_flip($values));
    }

    return [];
  }

  /**
   * @param  string  $field
   * @param  string|null  $key
   *
   * @return array
   */
  public function getValues(string $field, string $key = NULL) {
    $values = [];

    if ($fields = $this->get($field)) {
      foreach ($fields->getIterator() as $fieldEntity) {
        if ($data = $fieldEntity->getValue()) {
          if (is_string($key)) {
            if (isset($data[$key])) {
              $values[] = $data[$key];
            }
          }
          else {
            $values[] = $data;
          }
        }
      }
    }

    return $values;
  }

  /**
   * @param  string  $field
   * @param  string|null  $key
   * @param  string  $type
   * @param  string  $format
   *
   * @return array
   */
  public function formatDate(string $field, string $key = NULL, string $type = 'medium', string $format = '') {
    $datesFormatted = [];

    if ($fieldsDate = $this->get($field)) {
      foreach ($fieldsDate->getIterator() as $index => $fieldDate) {
        if ($fieldDate->{$key} instanceof DrupalDateTime) {
          $datesFormatted[$index] = \Drupal::service('date.formatter')
            ->format($fieldDate->{$key}->getTimestamp(), $type, $format);
        }
      }
    }

    return $datesFormatted;
  }

  /**
   * Rendu du champ
   *
   * @param $field
   *
   * @return string
   */
  public function getString($field) {
    if ($field = $this->get($field)) {
      return $field->getString();
    }

    return NULL;
  }

  /**
   * @param  string  $field
   *
   * @return \Drupal\field\Entity\FieldConfig
   */
  public function getConfig(string $field) {
    return FieldConfig::loadByName($this->entity()->getEntityTypeId(), $this->entity()->bundle(), self::format($field));
  }

  /**
   * @param  string  $field
   *
   * @return array|null
   */
  public function getDisplayConfig(string $field) {
    $formDisplay = \Drupal::entityTypeManager()
      ->getStorage('entity_form_display')
      ->load($this->entity()->getEntityTypeId().'.'.$this->entity()->bundle().'.default');

    /** @var \Drupal\Core\Entity\EntityDisplayBase $formDisplay * */
    return $formDisplay?->getComponent(self::format($field));
  }

}
