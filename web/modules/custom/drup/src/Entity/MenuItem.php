<?php

namespace Drupal\drup\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\drup\Helper\DrupUrl;
use Drupal\menu_link_content\Entity\MenuLinkContent;

/**
 * Element de Menu
 */
class MenuItem {

  /**
   * @var \Drupal\menu_link_content\Entity\MenuLinkContent
   */
  protected MenuLinkContent $entity;

  /**
   * @param  \Drupal\menu_link_content\Entity\MenuLinkContent  $entity
   */
  public function __construct(MenuLinkContent $entity) {
    $this->entity = $entity;
  }

  /**
   * @param  \Drupal\Core\Entity\EntityInterface  $entity
   *
   * @return static|null
   */
  public static function createFromContentEntity(EntityInterface $entity) {
    $menuLinkManager = \Drupal::service('plugin.manager.menu.link');
    $links = $menuLinkManager->loadLinksByRoute($entity->toUrl()
      ->getRouteName(), $entity->toUrl()->getRouteParameters());

    if (!empty($links)) {
      $link = key($links);

      return self::createFromPluginId($link);
    }

    return NULL;
  }

  /**
   * @param  string  $id
   *
   * @return static
   */
  public static function createFromPluginId(string $id) {
    $menu_link = explode(':', $id, 2);
    $uuid = $menu_link[1] ?? $id;

    return new static(\Drupal::service('entity.repository')
      ->loadEntityByUuid('menu_link_content', $uuid));
  }

  /**
   * @return \Drupal\Core\Entity\EntityInterface|null
   */
  public function getTargetEntity() {
    if ($url = $this->entity()->getUrlObject()) {
      return DrupUrl::loadEntity($url);
    }

    return NULL;
  }

  /**
   * @return \Drupal\menu_link_content\Entity\MenuLinkContent
   */
  public function entity() {
    return $this->entity;
  }

  /**
   * Parents
   *
   * @param  bool  $excludeCurrent
   *
   * @return \Drupal\drup\Entity\MenuItem[]
   */
  public function getParents(bool $excludeCurrent = TRUE): array {
    $items = [];

    /** @var \Drupal\Core\Menu\MenuLinkManager $menuLinkManager */
    $menuLinkManager = \Drupal::service('plugin.manager.menu.link');
    $links = $menuLinkManager->getParentIds($this->entity()->getPluginId());

    if ($excludeCurrent) {
      array_shift($links);
    }
    $links = array_reverse($links);

    if (!empty($links)) {
      foreach ($links as $pluginId => $link) {
        $items[] = self::createFromPluginId($pluginId);
      }
    }

    return $items;
  }

  /**
   * Frères
   *
   * @param  bool  $excludeCurrent
   *
   * @return \Drupal\drup\Entity\MenuItem[]
   */
  public function getSiblings(bool $excludeCurrent = TRUE): array {
    $menu = Menu::create()->setMenuName($this->entity()->getMenuName());

    if ($parent = $this->getParent()) {
      $menu->applyParameters($parent->entity()->getPluginId(), 1);
    }
    else {
      $menu->applyParameters(NULL, 1);
    }
    $menu->applyMenuTree();

    $items = $menu->getItems();

    if ($excludeCurrent === TRUE && !empty($items)) {
      foreach ($items as $index => $item) {
        if ($item->entity()->getPluginId() === $this->entity()->getPluginId()) {
          unset($items[$index]);
          break;
        }
      }
    }

    return $items;
  }

  /**
   * @param  \Drupal\menu_link_content\Entity\MenuLinkContent  $entity
   *
   * @return static
   */
  public static function create(MenuLinkContent $entity) {
    return new static($entity);
  }

  /**
   * Parent direct
   *
   * @return $this|null
   */
  public function getParent() {
    if ($parentId = $this->entity()->getParentId()) {
      return self::createFromPluginId($parentId);
    }

    return NULL;
  }

  /**
   * Enfants
   *
   * @param  int  $maxDepth
   *
   * @return \Drupal\drup\Entity\MenuItem[]
   */
  public function getChildren(int $maxDepth = 1): array {
    $menu = Menu::createFromMenuItem($this);
    $menu->applyParameters(NULL, $maxDepth);
    $menu->applyMenuTree();

    return $menu->getItems();
  }

}
