<?php

namespace Drupal\drup\Entity;

use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\ByteSizeMarkup;
use Drupal\image\Entity\ImageStyle;
use Drupal\responsive_image\Entity\ResponsiveImageStyle;

/**
 *
 */
class File {

  /**
   * @var File
   */
  protected $entity;

  /**
   * @var string
   */
  protected $uri;

  /**
   * @var \Drupal\Core\Image\Image $image
   */
  protected $image;

  /**
   * @param  \Drupal\file\Entity\File  $fileEntity
   */
  public function __construct(\Drupal\file\Entity\File $fileEntity) {
    $this->entity = $fileEntity;
    $this->uri = $fileEntity->getFileUri();
    $this->image = \Drupal::service('image.factory')->get($this->uri);
  }

  /**
   * Instancie selon un id de File
   *
   * @param  string  $fid
   *
   * @return \Drupal\drup\Entity\File
   */
  public static function createFromFid(string $fid) {
    if (($file = \Drupal\file\Entity\File::load($fid)) && $file instanceof \Drupal\file\Entity\File) {
      return new static($file);
    }

    return NULL;
  }

  /**
   * Rendu HTML d'un fichier SVG
   *
   * @param  string  $mediaUrl
   *
   * @return \Drupal\Component\Render\MarkupInterface|null
   */
  public static function renderSVG(string $mediaUrl) {
    $output = NULL;

    if ($svgContent = self::getSVGContent($mediaUrl)) {
      $output = Markup::create($svgContent);
    }

    return $output;
  }

  /**
   * Contenu d'un fichier SVG
   *
   * @param  string  $mediaUrl
   *
   * @return string|null
   */
  public static function getSVGContent(string $url) {
    $output = NULL;

    if ($mediaContent = @file_get_contents($url)) {
      $output = preg_replace('/<!--.*?-->/ms', '', $mediaContent);
      $output = str_replace('<?xml version="1.0" encoding="UTF-8"?>', '', $output);
    }

    return $output;
  }

  /**
   * @param  \Drupal\file\Entity\File  $fileEntity
   *
   * @return static
   */
  public static function create(\Drupal\file\Entity\File $fileEntity) {
    return new static($fileEntity);
  }

  /**
   * Défini le fichier en statut permanent
   *
   * @param $fid
   *
   * @return mixed
   */
  public static function setPermanent($fid) {
    if (\is_array($fid)) {
      $fid = current($fid);
    }

    if (($file = \Drupal\file\Entity\File::load($fid)) && $file instanceof \Drupal\file\Entity\File && $file->isTemporary()) {
      $file->setPermanent();
      $file->save();

      return $file;
    }

    return FALSE;
  }

  /**
   * Url absolue d'une image avec style d'image
   *
   * @param  string|null  $style
   *
   * @return string|null
   */
  public function getUrl(string $style = NULL) {
    $url = NULL;

    if ($style !== NULL && $this->isValid() && ($imageStyle = self::getImageStyleEntity($style))) {
      $url = $imageStyle->buildUrl($this->uri());
    }
    else {
      $url = \Drupal::service('file_url_generator')
        ->generateAbsoluteString($this->uri());
    }

    return $url;
  }

  /**
   * Fichier existant
   *
   * @return bool
   */
  public function isValid(): bool {
    return $this->image()->isValid();
  }

  /**
   * @return \Drupal\Core\Image\Image
   */
  public function image() {
    return $this->image;
  }

  /**
   * Entité ImageStyle ou ResponsiveImageStyle
   *
   * @param $style
   * @param  bool  $allowResponsiveImageStyle
   *
   * @return \Drupal\image\Entity\ImageStyle|\Drupal\responsive_image\Entity\ResponsiveImageStyle|null
   */
  public static function getImageStyleEntity(string $style, bool $allowResponsiveImageStyle = TRUE) {
    if ($allowResponsiveImageStyle && ($responsiveImageStyle = ResponsiveImageStyle::load($style)) && $responsiveImageStyle instanceof ResponsiveImageStyle) {
      return $responsiveImageStyle;
    }

    if (($imageStyle = ImageStyle::load($style)) && $imageStyle instanceof ImageStyle) {
      return $imageStyle;
    }

    return NULL;
  }

  /**
   * @return string
   */
  public function uri() {
    return $this->uri;
  }

  /**
   * @return array
   */
  public function getInfo() {
    $size = $this->entity()->getSize();
    $sizeHuman = ByteSizeMarkup::create($size);

    return [
      'size' => $size,
      'size_human' => $sizeHuman,
      'mime' => $this->entity()->getMimeType(),
      'mime_human' => explode('/', $this->entity()->getMimeType())[1],
      'name' => $this->entity()->getFilename(),
    ];
  }

  /**
   * @return \Drupal\file\Entity\File
   */
  public function entity() {
    return $this->entity;
  }

  /**
   * @param  null  $style
   * @param  array  $attributes
   *
   * @return array
   */
  public function renderImage($style = NULL, array $attributes = []) {
    $rendererOptions = [];

    if (!$this->isValid()) {
      return $rendererOptions;
    }

    // Render with image style
    if (!empty($style)) {
      if ($imageStyle = self::getImageStyleEntity($style, TRUE)) {
        $rendererOptions['#uri'] = $this->uri;
        $rendererOptions['#width'] = $this->image()->getWidth();
        $rendererOptions['#height'] = $this->image()->getHeight();

        if ($imageStyle instanceof ResponsiveImageStyle) {
          $rendererOptions += [
            '#theme' => 'responsive_image',
            '#responsive_image_style_id' => $style,
          ];
        }
        else {
          $rendererOptions += [
            '#theme' => 'image_style',
            '#style_name' => $style,
          ];
        }
      }
      else {
        \Drupal::messenger()
          ->addMessage('Le style d\'image '.$style.' n\'existe pas.', 'error');

        return $rendererOptions;
      }
    }
    // Render original image
    else {
      $rendererOptions += [
        '#theme' => 'image',
        '#uri' => $this->uri,
        '#width' => $this->image()->getWidth(),
        '#height' => $this->image()->getHeight(),
      ];
    }

    $rendererOptions['#attributes'] = [];
    if (!empty($attributes)) {
      $rendererOptions['#attributes'] = array_merge_recursive($rendererOptions['#attributes'], $attributes);
    }

    return $rendererOptions;
  }

}
