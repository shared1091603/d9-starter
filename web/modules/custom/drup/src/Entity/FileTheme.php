<?php

namespace Drupal\drup\Entity;

/**
 *
 */
class FileTheme {

  /**
   * @var string
   */
  protected $url;

  /**
   * @var string
   */
  protected $uri;

  /**
   * @var \Drupal\Core\Theme\ActiveTheme
   */
  protected $theme;

  /**
   * @var \Drupal\Core\Image\Image $image
   */
  protected $image;

  /**
   * @param  \Drupal\file\Entity\File  $fileEntity
   */
  public function __construct(string $url) {
    $this->url = $url;
    $this->uri = self::getThemeFileUri($url, NULL, FALSE);
    $this->image = \Drupal::service('image.factory')->get($this->uri);
  }

  /**
   * Chemin d'un fichier d'un thème
   *
   * @param  string  $relative_path  Chemin relatif au thème du fichier
   * @param  string|null  $theme  Nom du thème.
   * @param  bool  $absolute  Formattage de l'url en absolu ou relatif. /!\ Les
   *   urls absolues sont mal supportées avec les htpasswd
   *
   * @return string
   */
  public static function getThemeFileUri(string $relative_path, string $theme = NULL, bool $absolute = TRUE): string {
    $themeHandler = \Drupal::service('theme_handler');
    $theme = $theme ?? $themeHandler->getDefault();

    if ($relative_path[0] !== '/') {
      $relative_path = '/'.$relative_path;
    }

    $url = $themeHandler->getTheme($theme)->getPath().$relative_path;

    return $absolute ? \Drupal::request()
      ->getUriForPath('/'.$url) : ('./'.$url);
  }

  /**
   * @param  string  $url
   *
   * @return static
   */
  public static function create(string $url) {
    return new static($url);
  }

  /**
   * Retourne des informations sur le logo du site
   *
   * @param  string  $type  ='svg' Type de fichier (svg ou png)
   * @param  array  $options  Surcharge des éléments retournés
   *
   * @return object
   */
  public static function getLogo(string $type = 'svg', array $options = []): object {
    $options = array_merge([
      'url' => NULL,
      'width' => NULL,
      'height' => NULL,
      'mimetype' => $type === 'png' ? 'image/png' : 'image/svg+xml',
    ], $options);

    if (empty($options['url'])) {
      $options['url'] = self::getThemeFileUri('/images/logo.'.$type);
    }

    if ($type === 'png' && !empty($options['url']) && empty($options['width']) && empty($options['height']) && ($size = @getimagesize($options['url']))) {
      $options['width'] = $size[0];
      $options['height'] = $size[1];
      $options['mimetype'] = $size['mime'];
    }

    return (object) $options;
  }

  /**
   * @param  array  $attributes
   *
   * @return array
   */
  public function render(array $attributes = []) {
    $rendererOptions = [];

    if (!$this->isValid()) {
      return $rendererOptions;
    }

    $rendererOptions += [
      '#theme' => 'image',
      '#uri' => $this->getCleanUri(),
      '#width' => $this->image()->getWidth(),
      '#height' => $this->image()->getHeight(),
    ];

    $rendererOptions['#attributes'] = [];
    if (!empty($attributes)) {
      $rendererOptions['#attributes'] = array_merge_recursive($rendererOptions['#attributes'], $attributes);
    }

    return $rendererOptions;
  }

  /**
   * Fichier existant
   *
   * @return bool
   */
  public function isValid(): bool {
    return $this->image()->isValid();
  }

  /**
   * @return \Drupal\Core\Image\Image
   */
  public function image(): \Drupal\Core\Image\Image {
    return $this->image;
  }

  /**
   * @return string
   */
  public function getCleanUri() {
    $uri = $this->uri();
    $uri = ltrim($uri, '/');
    $uri = ltrim($uri, './');

    return $uri;
  }

  /**
   * @return string
   */
  public function uri(): string {
    return $this->uri;
  }

}
