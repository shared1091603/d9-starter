<?php

namespace Drupal\drup\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

trait ContentEntityBaseTranslatableTrait {

  /**
   * @param  \Drupal\Core\Entity\ContentEntityInterface  $entity
   * @param $languageId
   *
   * @return bool
   */
  public static function isTranslated(ContentEntityInterface $entity, $languageId = NULL) {
    $isAllowed = TRUE;

    if (method_exists($entity, 'getTranslationLanguages')) {
      if ($languageId === NULL) {
        $languageId = \Drupal::languageManager()->getCurrentLanguage()->getId();
      }

      $translations = $entity->getTranslationLanguages();

      if (!isset($translations['und']) && !$entity->hasTranslation($languageId)) {
        $isAllowed = FALSE;
      }
    }

    return $isAllowed;
  }

  /**
   * @inheritDoc
   */
  public static function load($id) {
    /** @var ContentEntityInterface $entity */
    if ($entity = parent::load($id)) {
      return $entity->translate();
    }

    return NULL;
  }

  public function translate($languageId = NULL) {
    if ($languageId === NULL) {
      $languageId = \Drupal::languageManager()->getCurrentLanguage()->getId();
    }

    if ($this->hasTranslation($languageId)) {
      return $this->getTranslation($languageId);
    }

    return $this;
  }

  /**
   * @inheritDoc
   */
  public static function loadMultiple(array $ids = NULL) {
    if ($entities = parent::loadMultiple($ids)) {
      /** @var ContentEntityInterface $entity */
      foreach ($entities as &$entity) {
        $entity = $entity->translate();
      }

      return $entities;
    }

    return [];
  }

}
