<?php

namespace Drupal\drup\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Tags;
use Drupal\Core\Entity\EntityAutocompleteMatcher;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Class DrupEntityAutocompleteMatcher
 *
 * @package Drupal\drup\Entity
 */
class DrupEntityAutocompleteMatcher extends EntityAutocompleteMatcher {

  /**
   * @inheritDoc
   */
  public function getMatches($target_type, $selection_handler, $selection_settings, $string = '') {
    //$matches = parent::getMatches($target_type, $selection_handler, $selection_settings, $string);
    $matches = [];

    $options = $selection_settings + [
        'target_type' => $target_type,
        'handler' => $selection_handler,
      ];
    $handler = $this->selectionManager->getInstance($options);

    if (isset($string)) {
      $language = \Drupal::languageManager()->getCurrentLanguage()->getId();

      // Get an array of matching entities.
      $match_operator = !empty($selection_settings['match_operator']) ? $selection_settings['match_operator'] : 'CONTAINS';
      $match_limit = isset($selection_settings['match_limit']) ? (int) $selection_settings['match_limit'] : 10;
      $entity_labels = $handler->getReferenceableEntities($string, $match_operator, $match_limit);

      // Loop through the entities and convert them into autocomplete output.
      foreach ($entity_labels as $values) {
        foreach ($values as $entity_id => $label) {
          $entity = \Drupal::entityTypeManager()
            ->getStorage($target_type)
            ->load($entity_id);
          $entity = \Drupal::service('entity.repository')
            ->getTranslationFromContext($entity, $language);

          $custom_label = [];

          $type = !empty($entity->type->entity) ? $entity->type->entity->label() : $entity->bundle();
          $custom_label[] = ucfirst(t($type));

          if ($entity instanceof EntityPublishedInterface) {
            $custom_label[] = ucfirst(t($entity->isPublished() ? 'published' : 'unpublished'));
          }

          $key = "$label ($entity_id)";
          // Strip things like starting/trailing white spaces, line breaks and
          // tags.
          $key = preg_replace('/\s\s+/', ' ', str_replace("\n", '', trim(Html::decodeEntities(strip_tags($key)))));
          // Names containing commas or quotes must be wrapped in quotes.
          $key = Tags::encode($key);

          $label = $label.' ['.implode(', ', $custom_label).']';

          $matches[] = ['value' => $key, 'label' => $label];
        }
      }
    }

    if (!isset($matches)) {
      $matches = parent::getMatches($target_type, $selection_handler, $selection_settings, $string);
    }

    return $matches;
  }

}
