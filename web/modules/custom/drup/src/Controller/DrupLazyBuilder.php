<?php

namespace Drupal\drup_site\Controller;

use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Implémentation du #lazy_builder pour améliorer la gestion du cache sur
 * certaines pages
 */
class DrupLazyBuilder implements TrustedCallbackInterface {

  /**
   * @inheritDoc
   */
  public static function trustedCallbacks() {
    return [];
  }

  /**
   * @return string[]
   *
   * @example (
   *   $variables['content'] = [
   *    '#lazy_builder' => ['drup_site.callbacks:buildAdvicesView', []],
   *    '#create_placeholder' => TRUE,
   *   ];
   * )
   */
  public static function _example() {
    // ajax ne fonctionne pas sans passer par #markup pour du lazy loading
    $view = views_embed_view('advices', 'list_push');
    $build = [
      '#markup' => \Drupal::service('renderer')->render($view),
    ];

    return $build;
  }

}
