<?php

namespace Drupal\drup\Helper;

use Drupal\Component\Utility\Unicode;

/**
 * Class DrupString
 *
 * Méthodes globales pour le traitement des chaines de caractères
 *
 * @package Drupal\drup\Helper
 */
abstract class DrupString {

  /**
   * Tronquer
   *
   * @param $string
   * @param  int  $maxLength
   * @param  string|bool  $stripTags  false pour désactiver
   *
   * @return string
   */
  public static function truncate($string, $maxLength = 250, $stripTags = NULL) {
    if ($stripTags !== FALSE) {
      $string = strip_tags($string, $stripTags);
    }

    $string = str_replace(PHP_EOL, '', $string);

    if ($maxLength > 0) {
      $string = Unicode::truncate($string, $maxLength, TRUE, TRUE);
    }

    return $string;
  }

  /**
   * Formatage d'un numéro de téléphone
   *
   * @param  string  $phone  Numéro
   * @param  string  $separator  Séparateur des nombres
   *
   * @return string
   */
  public static function formatPhoneNumber($phone, $separator = ' ') {
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();

    switch ($langcode) {
      case 'fr':
      default:
        $split = str_split($phone, 2);

        if (strncmp($phone, '+', 1) === 0) {
          $split[0] .= substr($split[1], 0, 1);
          $split[1] = substr($split[1], 1);
        }
        break;
    }

    if (!empty($split)) {
      $phone = implode($separator, $split);
    }

    return $phone;
  }

}
