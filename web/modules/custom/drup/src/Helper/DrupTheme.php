<?php

namespace Drupal\drup\Helper;

/**
 * Class DrupTheme
 *
 * @package Drupal\drup\Helper
 */
abstract class DrupTheme {

  /**
   * Format $themes
   *
   * @param $themes
   * @param  array  $options
   */
  public static function format(&$themes, $options = []) {
    $options = \array_merge([
      'type' => 'blocks',
    ], $options);

    $themePath = \Drupal::theme()->getActiveTheme()->getPath();
    $themePathBlocks = $themePath.'/templates/'.$options['type'];

    foreach ($themes as $themeId => &$theme) {
      $template = NULL;

      if (str_contains($themeId, 'drup_'.$options['type'])) {
        // Admin
        if (str_contains($themeId, 'drup_'.$options['type'].'_admin')) {
          $template = \str_replace('drup_'.$options['type'].'_admin_', '', $themeId);
          $theme['variables']['admin_url'] = NULL;
        }
        else {
          $template = \str_replace('drup_'.$options['type'].'_', '', $themeId);
        }
      }

      if ($template === NULL) {
        $template = \str_replace('drup_', '', $themeId);
      }

      if (!isset($theme['template'])) {
        $theme['path'] = $themePathBlocks;
        $theme['template'] = \str_replace('_', '-', $template);
      }
      $theme['variables']['theme_path'] = $themePath;
    }
    unset($theme);
  }

}
