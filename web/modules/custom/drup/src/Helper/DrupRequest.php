<?php

namespace Drupal\drup\Helper;

use Symfony\Component\HttpFoundation\Request;

/**
 * Class DrupRequest
 *
 * @package Drupal\drup\Helper
 */
abstract class DrupRequest {

  /**
   * @return mixed
   */
  public static function getTitle() {
    $request = \Drupal::request();
    $route = \Drupal::routeMatch()->getRouteObject();

    return \Drupal::service('title_resolver')->getTitle($request, $route);
  }

  /**
   * @return string
   */
  public static function getSiteName() {
    return \Drupal::config('system.site')->get('name');
  }

  /**
   * @param  \Symfony\Component\Routing\Route|null  $route
   *
   * @return mixed
   */
  public static function isAdminRoute(\Symfony\Component\Routing\Route $route = NULL) {
    $adminContext = \Drupal::service('router.admin_context');

    if ($route === NULL) {
      $route = \Drupal::routeMatch()->getRouteObject();
    }

    return $adminContext->isAdminRoute($route);
  }

  /**
   * @return array
   */
  public static function getArgs() {
    return \Drupal::routeMatch()->getParameters()->all();
  }

  /**
   * Nom de la route de la page courante hors contexte ajax (ex : dans un view
   * ajax)
   *
   * @return string|null
   */
  public static function getRefererRouteName() {
    $previousUrl = \Drupal::request()->server->get('HTTP_REFERER');
    $fakeRequest = Request::create($previousUrl);

    /** @var \Drupal\Core\Url $url */
    if ($url = \Drupal::service('path.validator')
      ->getUrlIfValid($fakeRequest->getRequestUri())) {
      return $url->getRouteName();
    }

    return NULL;
  }

  /**
   * @return string|null
   */
  public static function getRouteName() {
    return \Drupal::routeMatch()->getRouteName();
  }

  /**
   * @return bool
   */
  public static function isFront() {
    return \Drupal::service('path.matcher')->isFrontPage();
  }

  /**
   * Récupère les paramètres de l'url précédente
   *
   * @return array
   */
  public static function getPreviousRouteParameters() {
    if (($previousUrl = \Drupal::request()->server->get('HTTP_REFERER')) && $fakeRequest = Request::create($previousUrl)) {
      /** @var \Drupal\Core\Url $url * */
      $url = \Drupal::service('path.validator')
        ->getUrlIfValid($fakeRequest->getRequestUri());

      if ($url) {
        return $url->getRouteParameters();
      }
    }

    return [];
  }

}
