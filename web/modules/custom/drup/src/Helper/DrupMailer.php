<?php

namespace Drupal\drup;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\symfony_mailer\Address;

/**
 * Utils pour "simplifier" l'envoi de mail via le module symfony_mailer
 *
 * @example (
 *   $address = new \Drupal\symfony_mailer\Address($to, $identity,
 *   \Drupal::languageManager()->getCurrentLanguage()->getId());
 *   $email =
 *   DrupMailer::create('drup_site_catalog_entities__catalog_entity_share_mail',
 *   $address, $subject);
 *    $email->getEmailFactory()->setBody([
 *        '#theme' => 'drup_site_catalog_entities__catalog_entity_share_mail',
 *        '#data' => $email::mergeBodyData([
 *            'identity' => $identity,
 *            'email' => $to,
 *            'entities' => $entitiesRendered
 *        ])
 *    ]);
 *   $email->getEmailFactory()->setVariable('body_no_row', true);
 *   $results['sent'] = (bool) $email->send();
 * )
 *
 */
class DrupMailer {

  /**
   * @var \Drupal\symfony_mailer\EmailInterface $emailFactory
   */
  protected $emailFactory;

  /**
   * @param  string  $mail_id
   * @param $to
   * @param  \Drupal\Core\StringTranslation\TranslatableMarkup|string|null  $subjet
   */
  public function __construct(string $mail_id, $to = NULL, TranslatableMarkup|string $subjet = NULL) {
    $this->emailFactory = \Drupal::service('email_factory')
      ->newTypedEmail('symfony_mailer', $mail_id);

    if (!empty($to)) {
      if (is_string($to)) {
        $to = new Address($to, NULL, \Drupal::languageManager()
          ->getCurrentLanguage()
          ->getId());
      }
      $this->emailFactory->setTo($to);
    }
    if ($subjet !== NULL) {
      $this->emailFactory->setSubject($subjet);
    }
  }

  /**
   * @param  string  $mail_id
   * @param  null  $to
   * @param  \Drupal\Core\StringTranslation\TranslatableMarkup|string|null  $subjet
   *
   * @return static
   */
  public static function create(string $mail_id, $to = NULL, TranslatableMarkup|string $subjet = NULL) {
    return new static($mail_id, $to, $subjet);
  }

  /**
   * Insère des valeurs par défaut dans le template dy body du mail
   *
   * @param  array  $data
   * @param  string|null  $langcode
   *
   * @return array
   * @example(
   *   $email->getEmailFactory()->setBody([
   *       '#theme' => 'drup_site_catalog_entities__catalog_entity_share_mail',
   *       '#data' => $email::mergeBodyData([
   *           'identity' => $identity,
   *           'email' => $to,
   *           'entities' => $entitiesRendered
   *       ])
   *   ]);
   * )
   *
   */
  public static function mergeBodyData(array $data, string $langcode = NULL) {
    if ($langcode === NULL) {
      $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    }
    $default = [
      'langcode' => $langcode,
      'defaults' => [],
    ];

    return array_merge_recursive($default, $data);
  }

  /**
   * @return \Drupal\symfony_mailer\EmailInterface
   */
  public function getEmailFactory() {
    return $this->emailFactory;
  }

  /**
   * @return bool
   */
  public function send() {
    return $this->emailFactory->send();
  }

}
