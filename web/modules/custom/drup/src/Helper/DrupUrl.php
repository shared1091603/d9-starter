<?php

namespace Drupal\drup\Helper;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DrupUrl
 *
 * Méthodes globales pour le traitement des urls
 *
 * @package Drupal\drup\Helper
 */
abstract class DrupUrl {

  /**
   * @param $string
   * @param  array  $options
   *
   * @return \Drupal\Core\Url
   */
  public static function inputToUrl($string, array $options = []) {
    if (UrlHelper::isExternal($string)) {
      return Url::fromUri($string, $options);
    }

    return Url::fromUserInput($string, $options);
  }

  /**
   * Remplacement d'un argument dans la queryString
   *
   * @param $argument
   * @param $value
   * @param $queryString
   *
   * @return string
   */
  public static function replaceArgument($argument, $value, $queryString): string {
    $separator = !empty($queryString) ? '&' : NULL;
    $replace = (!empty($queryString) && str_contains($queryString, $argument));

    return '?'.($replace ? preg_replace('/'.$argument.'\=[a-z0-9]+/i', $argument.'='.$value, $queryString) : $queryString.$separator.$argument.'='.$value);
  }

  /**
   * Retourne le chemin absolu
   *
   * @param  null  $relativePath
   * @param  null  $baseUrl
   *
   * @return string
   */
  public static function getAbsolutePath($relativePath = NULL, $baseUrl = NULL) {
    if ($baseUrl === NULL) {
      $baseUrl = Request::createFromGlobals()->getSchemeAndHttpHost();
    }
    if ($relativePath === NULL) {
      $relativePath = \Drupal::service('path.current')->getPath();
    }

    return $baseUrl.$relativePath;
  }

  /**
   * Load l'entité de contenu Drupal depuis une entité Url
   *
   * @param  \Drupal\Core\Url  $url
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function loadEntity(Url $url) {
    if (!$url->isExternal() && $url->isRouted()) {
      $urlParameters = $url->getRouteParameters();

      if (!empty($urlParameters)) {
        $entityType = key($urlParameters);
        $entityId = current($urlParameters);

        return \Drupal::entityTypeManager()
          ->getStorage($entityType)
          ->load($entityId);
      }
    }

    return NULL;
  }

}
