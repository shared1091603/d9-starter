<?php

namespace Drupal\drup\OEmbed;

use Drupal\media\OEmbed\Resource;
use Drupal\media\OEmbed\ResourceFetcher as MediaResourceFetcher;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Overrides the oEmbed resources.
 *
 * This class override the media.oembed.resource_fetcher service from the core
 * media module.
 *
 * @see \Drupal\media\OEmbed\ResourceFetcher
 */
class ResourceFetcher extends MediaResourceFetcher {

  /**
   * @inheritDoc
   *
   * @see https://www.drupal.org/project/drupal/issues/3042423
   * @see https://www.drupal.org/project/drupal/issues/3043821#comment-13432599
   *
   */
  protected function createResource(array $data, $url) {
    if ($data['type'] === Resource::TYPE_VIDEO && $data['provider_name'] === 'YouTube') {
      // Replace the default youtube domain with the no-cookie domain.
      $data['html'] = str_replace('youtube.com/', 'youtube-nocookie.com/', $data['html']);

      // Check if there is a larger available thumbnail size.
      if (str_contains($data['thumbnail_url'], 'hqdefault.jpg')) {
        // Array of thumbnail sizes above 'hqdefault' to try, starting with the largest size.
        $thumbnailTypes = [
          'maxresdefault' => [
            'width' => 1280,
            'height' => 720,
          ],
          'sddefault' => [
            'width' => 640,
            'height' => 480,
          ],
        ];

        foreach ($thumbnailTypes as $thumbnailName => $thumbnailDimensions) {
          // Replace 'hqdefault' in the thumbnail URL with the current type we're testing for.
          $testThumbnailURL = str_replace('hqdefault', $thumbnailName, $data['thumbnail_url']);
          // We need to wrap the request in a try {} catch {} because Guzzle will throw an exception on a 404.
          try {
            $response = $this->httpClient->request('GET', $testThumbnailURL);
            // Got an exception? Skip to the next thumbnail size, assuming this returned a 404 or ran into some other error.
          }
          catch (GuzzleException $e) {
            continue;
          }

          // If this was a 200 response, update the thumbnail URL and dimensions
          // with the higher resolution and break out of the loop.
          if ($response->getStatusCode() === 200) {
            $data['thumbnail_url'] = $testThumbnailURL;
            $data['thumbnail_width'] = $thumbnailDimensions['width'];
            $data['thumbnail_height'] = $thumbnailDimensions['height'];
            break;
          }
        }
      }
    }

    // Create the resource.
    return parent::createResource($data, $url);
  }

}
