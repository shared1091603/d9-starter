<?php

namespace Drupal\drup;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Routing\RouteObjectInterface;
use Drupal\system\PathBasedBreadcrumbBuilder;

/**
 * Adds the current page title to the breadcrumb.
 *
 * Extend PathBased Breadcrumbs to include the current page title as an unlinked
 * crumb. The module uses the path if the title is unavailable and it excludes
 * all admin paths.
 *
 * {@inheritdoc}
 */
class DrupBreadcrumbBuilder extends PathBasedBreadcrumbBuilder {

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $breadcrumbs = parent::build($route_match);

    $request = \Drupal::request();
    $path = trim($this->context->getPathInfo(), '/');
    $path_elements = explode('/', $path);
    $route = $request->attributes->get(RouteObjectInterface::ROUTE_OBJECT);

    // Ne modifiez pas le fil d'Ariane sur les chemins d'administration.
    if ($route && !$route->getOption('_admin_route')) {
      $title = $this->titleResolver->getTitle($request, $route);
      if (!isset($title)) {
        // Retour à l'utilisation du composant de chemin brut comme titre si il manque un attribut _title ou _title_callback à la route.
        $title = str_replace([
          '-',
          '_',
        ], ' ', Unicode::ucfirst(end($path_elements)));
      }
      $breadcrumbs->addLink(Link::createFromRoute($title, '<none>'));
    }

    // Ajoutez le chemin URL complet comme contexte de cache, car nous afficherons la page en cours dans le fil d'Ariane.
    $breadcrumbs->addCacheContexts(['url.path']);

    return $breadcrumbs;
  }

}
