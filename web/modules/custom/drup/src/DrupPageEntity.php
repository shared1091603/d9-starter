<?php

namespace Drupal\drup;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\drup\Helper\DrupRequest;

/**
 * Class DrupPageEntity
 *
 * @package Drupal\drup
 */
class DrupPageEntity {

  /**
   * @var array
   */
  protected $routeParameters;

  /**
   * @var string
   */
  protected $routeSource;

  /**
   * @var EntityInterface
   */
  protected $entity;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * DrupPageEntity constructor.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;

    if (in_array(DrupRequest::getRouteName(), [
      'views.ajax',
      //'entity.node.preview',
    ])) {
      $this->routeParameters = DrupRequest::getPreviousRouteParameters();
      $this->routeSource = 'referer';
    }
    else {
      $this->routeParameters = DrupRequest::getArgs();
      $this->routeSource = 'current';
    }

    $this->entity = $this->loadEntity();
  }

  /**
   * @return \Drupal\Core\Entity\EntityInterface|null
   */
  protected function loadEntity() {
    if (!empty($this->routeParameters)) {
      // Route Normale : retourne la première entité dans la route
      if ($this->routeSource === 'current') {
        foreach ($this->routeParameters as $index => $routeParameter) {
          if ($routeParameter instanceof EntityInterface) {
            return $routeParameter;
          }
          if (!empty($this->entityTypeManager->getDefinition($index, FALSE))) {
            return $this->entityTypeManager->getStorage($index)
              ->load($routeParameter);
          }
        }
      }
      // Route Previous
      else {
        // Reproduit les data de $this->routeSource === 'current' pour mutualiser
        foreach ($this->routeParameters as $index => $routeParameter) {
          if (!empty($this->entityTypeManager->getDefinition($index, FALSE))) {
            $this->routeParameters[$index] = $this->entityTypeManager->getStorage($index)
              ->load($routeParameter);
          }
        }

        return current($this->routeParameters);
      }
    }

    return NULL;
  }

  /**
   * @return string|null
   */
  public function getEntityType(): ?string {
    return $this->getEntity()?->getEntityTypeId();
  }

  /**
   * @return EntityInterface|null
   */
  public function getEntity(): ?EntityInterface {
    return $this->entity;
  }

  /**
   * @param  EntityInterface  $entity
   *
   * @return bool
   */
  public function is(EntityInterface $entity): bool {
    return ($entity->bundle() === $this->getBundle() && (int) $entity->id() === (int) $this->id());
  }

  /**
   * @return string|null
   */
  public function getBundle(): ?string {
    return $this->getEntity()?->bundle();
  }

  /**
   * @return int|null
   */
  public function id() {
    return $this->getEntity()?->id();
  }

  /**
   * @return array
   */
  public function getRouteParameters() {
    return $this->routeParameters;
  }

}
