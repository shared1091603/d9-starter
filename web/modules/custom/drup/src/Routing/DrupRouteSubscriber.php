<?php

namespace Drupal\drup\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\drup\Controller\DrupEntityAutocompleteController;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class DrupRouteSubscriber
 *
 * @package Drupal\drup\Routing
 */
class DrupRouteSubscriber extends RouteSubscriberBase {

  /**
   * @param  \Symfony\Component\Routing\RouteCollection  $collection
   */
  public function alterRoutes(RouteCollection $collection) {
    // Entity autocomplete
    if ($route = $collection->get('system.entity_autocomplete')) {
      $route->setDefault('_controller', DrupEntityAutocompleteController::class.'::handleAutocomplete');
    }

    // Rétablir les termes par ordre alphabétique
    if ($route = $collection->get('entity.taxonomy_vocabulary.reset_form')) {
      $route->setRequirement('_permission', 'access taxonomy overview');
    }
  }

}
