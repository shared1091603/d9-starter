<?php

namespace Drupal\drup\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;

/**
 * Class DrupBlockBase
 *
 * @package Drupal\drup\Block
 */
abstract class DrupBlockBase extends BlockBase {

  /**
   * @inheritDoc
   */
  public function build() {}

  /**
   * @param  array  $parameters
   *
   * @return array
   */
  public function mergeBuildParameters($parameters = []) {
    $parameters = array_merge_recursive($parameters, []);

    return $parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), []);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return Cache::mergeTags(parent::getCacheTags(), []);
  }

}
