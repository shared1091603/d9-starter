<?php

namespace Drupal\drup_settings;

use Drupal\language\Config\LanguageConfigOverride;
use Drupal\language\ConfigurableLanguageManager;

/**
 * Class DrupSettings
 *
 * @package Drupal\drup_settings
 */
class DrupSettings {

  /**
   * Nom du contexte spécifiant qu'une variable est disponible pour toutes les
   * langues
   *
   * @var string
   */
  public static string $contextNeutral = 'und';

  /**
   * Nom de la configuration des variables
   *
   * @var string
   */
  protected static string $configValuesName = 'drup_settings.values';

  /**
   * Nom de la configuration des contextes pour récupérer les variables
   *
   * @var string
   */
  protected static string $configContextsName = 'drup_settings.contexts';

  /**
   * @var LanguageConfigOverride
   */
  protected LanguageConfigOverride $configValues;

  /**
   * @var \Drupal\Core\Config\Config
   */
  protected \Drupal\Core\Config\Config $configContexts;

  /**
   * @var string
   */
  protected string $context;

  /**
   * @var ConfigurableLanguageManager
   */
  protected ConfigurableLanguageManager|\Drupal\Core\Language\LanguageManagerInterface $languageManager;

  /**
   * DrupSettings constructor.
   *
   * @param  null  $context
   */
  public function __construct(string $context = NULL) {
    $this->configContexts = \Drupal::service('config.factory')
      ->getEditable(self::getConfigContextsName());

    $this->languageManager = \Drupal::languageManager();
    $this->applyContext($context);
  }

  /**
   * Retourne le nom de la configuration des contextes
   *
   * @return string
   */
  public static function getConfigContextsName() {
    return self::$configContextsName;
  }

  /**
   * Applique un contexte à l'ensemble de la classe, et réinstancie la config
   * des variables en fonction
   *
   * @param  string|null  $context
   */
  public function applyContext(string $context = NULL) {
    if ($context === NULL) {
      $context = $this->languageManager->getCurrentLanguage()->getId();
    }
    $this->context = $context;

    if ($config = $this->getConfigValuesByContext($this->context)) {
      $this->configValues = $config;
    }
  }

  /**
   * Retourne une configuration contextualisée
   *
   * @param  string  $context
   *
   * @return \Drupal\language\Config\LanguageConfigOverride
   */
  protected function getConfigValuesByContext(string $context) {
    return $this->languageManager->getLanguageConfigOverride($context, self::getConfigValuesName());
  }

  /**
   * Retourne le nom de la configuration des variables
   *
   * @return string
   */
  public static function getConfigValuesName() {
    return self::$configValuesName;
  }

  /**
   * @return string[]
   */
  public static function getCacheTags() {
    return [
      'config:'.self::$configValuesName,
      'config:'.self::$configContextsName,
    ];
  }

  /**
   * Applique le contexte commun
   */
  public function applyNeutralContext() {
    $this->applyContext(self::$contextNeutral);
  }

  /**
   * Enregistre une variable dans la config contextualisée
   *
   * @param  string  $key
   * @param $value
   * @param  string|null  $context
   */
  public function setValue(string $key, $value, string $context = NULL) {
    $context = $this->getContext($key, $context);
    $config = ($context === $this->context) ? $this->getConfigValues() : $this->getConfigValuesByContext($context);
    $config->set($this->formatKey($key), $value)->save();
  }

  /**
   * Récupère le contexte à appliquer localement
   *
   * @param  string  $key
   * @param  string|null  $context
   *
   * @return string|null
   */
  protected function getContext(string $key, string $context = NULL) {
    if ($context === NULL) {
      $context = $this->getDefaultContext($this->formatKey($key));
    }
    if ($context === NULL) {
      $context = $this->context;
    }

    return $context;
  }

  /**
   * Récupère le contexte par défaut d'une variable
   *
   * @param  string  $key
   *
   * @return string|null
   */
  protected function getDefaultContext(string $key) {
    if ($data = $this->configContexts->get($key)) {
      return $data['context'];
    }

    return NULL;
  }

  /**
   * Formatte la clé d'une variable
   *
   * @param  string  $key
   *
   * @return string
   */
  protected function formatKey(string $key) {
    return $key;
  }

  /**
   * Retourne la configuration de DrupSettings contextualisée par le contexte
   * par défaut de la classe
   *
   * @return LanguageConfigOverride
   */
  protected function getConfigValues() {
    return $this->configValues;
  }

  /**
   * Enregistre le contexte par défaut de chaque variable de DrupSettings
   *
   * @param  array  $contexts
   *
   * @see \Drupal\drup_settings\Form\DrupSettingsForm
   */
  public function setContexts(array $contexts) {
    foreach ($contexts as $index => $context) {
      $this->configContexts->set($index, (array) $context);
    }
    $this->configContexts->save();
  }

  /**
   * @return array
   */
  public function getSocialNetworksUrl(string $context = NULL) {
    return $this->searchValues('site_social_link_', TRUE, $context);
  }

  /**
   * Recherche dans la config toutes les variables commençant par un motif
   *
   * @param  string  $pattern  Motif à cherche (exemple : contact)
   * @param  bool  $trimSearch  Enlève la valeur de $search des indexes du tableau
   *   de résultats (ex : contact_phone => phone)
   * @param  string|null  $context
   *
   * @return array
   */
  public function searchValues(string $pattern, bool $trimSearch = TRUE, string $context = NULL) {
    $values = [];

    $pattern = $this->formatKey($pattern);

    $contexts = $this->getContexts();
    foreach ($contexts as $key => $variable) {
      if (str_contains($key, $pattern)) {
        $variableContext = $context === NULL ? $variable['context'] : $this->getContext($this->formatKey($key), $context);
        $value = $this->getValue($key, $variableContext);

        if ($trimSearch === TRUE) {
          $key = trim(str_replace($pattern, '', $key), '_');
        }
        $values[$key] = $value;
      }
    }

    return $values;
  }

  /**
   * Liste du contexte de chaque variable
   *
   * @return \Drupal\Core\Config\Config
   */
  protected function getContexts() {
    return $this->configContexts->get();
  }

  /**
   * Récupère une variable en fonction d'un contexte (celui par défaut de la
   * variable ou forcé en spécifique)
   *
   * @param  string  $key  Clé de la variable
   * @param  string|null  $context  Force un contexte spécifique
   *
   * @return mixed
   */
  public function getValue(string $key, string $context = NULL) {
    $context = $this->getContext($key, $context);
    $config = ($context === $this->context) ? $this->getConfigValues() : $this->getConfigValuesByContext($context);

    return $config->get($this->formatKey($key));
  }

  /**
   * @param  string  $id
   * @param $context
   *
   * @return string|null
   */
  public function getSocialNetworkUrl(string $id, string $context = NULL) {
    return $this->getValue('site_social_link_'.$id, $context);
  }

}
