<?php

namespace Drupal\drup_settings\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\drup\Entity\File;
use Drupal\drup_settings\DrupSettings;
use Drupal\user\Entity\User;

/**
 * Class DrupSettingsFrom.
 */
class DrupSettingsForm extends ConfigFormBase {

  /**
   * @var DrupSettings
   */
  protected $drupSettings;

  /**
   * @var string
   */
  protected $formContainer = 'container';

  /**
   * Stock le contexte (langue) de DrupSettings de chaque form item
   *
   * @var array
   */
  protected $formItemsData = [];

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'drup_settings_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->drupSettings = \Drupal::service('drup.settings');

    $form[$this->formContainer] = [
      '#type' => 'horizontal_tabs',
      '#prefix' => '<div id="drup-settings-wrapper">',
      '#suffix' => '</div>',
      '#group_name' => 'drup.settings',
      '#entity_type' => 'drup.settings',
      '#bundle' => 'drup.settings',
    ];

    /**
     * MAIN SETTINGS
     */
    $form[$this->formContainer]['main'] = [
      '#type' => 'details',
      '#title' => $this->t('Global'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];

    $form[$this->formContainer]['main']['site_information'] = [
      '#type' => 'details',
      '#title' => $this->t('Site details'),
      '#open' => TRUE,
    ];

    /* SEO */
    $form[$this->formContainer]['main']['site_seo'] = [
      '#type' => 'details',
      '#title' => $this->t('Global SEO'),
      '#open' => TRUE,
    ];
    $form[$this->formContainer]['main']['site_seo']['site_logo_alt'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Logo alt attribute'),
      '#drup_context' => NULL,
    ];
    $form[$this->formContainer]['main']['site_seo']['site_tag_manager'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Analytics tracker ID'),
      '#drup_context' => NULL,
    ];

    $socialNetworks = static::$socialNetworks;
    if (!empty($socialNetworks)) {
      $form[$this->formContainer]['main']['social_networks'] = [
        '#type' => 'details',
        '#title' => $this->t('Social network urls'),
        '#open' => TRUE,
      ];
      foreach ($socialNetworks as $network => $title) {
        $form[$this->formContainer]['main']['social_networks']['site_social_link_'.$network] = [
          '#type' => 'textfield',
          '#title' => $title,
          '#drup_context' => NULL,
        ];
      }
    }

    $form[$this->formContainer]['emails'] = [
      '#type' => 'details',
      '#title' => $this->t('Emails'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];
    $form[$this->formContainer]['emails']['site_emails_from'] = [
      '#type' => 'email',
      '#title' => $this->t('Default email sender'),
      '#required' => TRUE,
      '#drup_context' => DrupSettings::$contextNeutral,
      '#description' => t("The address of the sender of automatic emails sent during registration and new password requests, and other notifications. (Use an address within the domain of your site to avoid being considered spam)."),
    ];
    $form[$this->formContainer]['emails']['site_emails_to'] = [
      '#type' => 'email',
      '#title' => $this->t('Default email recipient'),
      '#required' => TRUE,
      '#drup_context' => DrupSettings::$contextNeutral,
    ];

    /**
     * FEATURES
     */
    $isSuperAdmin = User::load(\Drupal::currentUser()->id())
      ->hasRole('super_admin');
    $form[$this->formContainer]['features'] = [
      '#type' => 'details',
      '#title' => $this->t('Features'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#disabled' => !$isSuperAdmin,
    ];
    $form[$this->formContainer]['features']['default_list_image'] = [
      '#type' => 'media_library',
      '#allowed_bundles' => ['image'],
      '#cardinality' => 1,
      '#title' => $this->t('Média représentant l\'image par défaut dans les listes de contenus'),
      '#drup_context' => DrupSettings::$contextNeutral,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * @inheritDoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    foreach ($values as $key => $value) {
      if (isset($this->formItemsData[$key])) {
        $this->drupSettings->setValue($key, $value, $this->formItemsData[$key]->context);

        // Files
        if (!empty($value) && in_array($this->formItemsData[$key]->type, ['managed_file'])) {
          File::setPermanent($value);
        }
      }
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      DrupSettings::getConfigValuesName(),
      DrupSettings::getConfigContextsName(),
    ];
  }

  /**
   * Ajoute les default_value + description des champs automatiquement selon la
   * clé #drup_context
   *
   * @param $form
   * @param $form_state
   */
  protected function populateDefaultValues(&$items, $form_state) {
    if (is_array($items)) {
      foreach ($items as $key => &$item) {
        if (is_array($item) && array_key_exists('#drup_context', $item) && empty($item['#default_value'])) {
          // Save info about current form item
          $this->formItemsData[$key] = (object) [
            'context' => $item['#drup_context'],
            'type' => $item['#type'],
          ];

          // Set default value
          $item['#default_value'] = $this->drupSettings->getValue($key, $item['#drup_context']);

          if ($item['#default_value'] !== NULL && $item['#type'] === 'entity_autocomplete') {
            $item['#default_value'] = \Drupal::entityTypeManager()
              ->getStorage($item['#target_type'])
              ->load($item['#default_value']);
          }
          elseif ($item['#type'] === 'text_format' && isset($item['#default_value']['value'])) {
            $item['#default_value'] = $item['#default_value']['value'];
          }

          // UX description
          $description = '<i>'.$this->t($item['#drup_context'] === DrupSettings::$contextNeutral ? 'Common to all languages' : 'Specific to each language').'</i>';
          if (!isset($item['#description'])) {
            $item['#description'] = $description;
          }
          else {
            $item['#description'] = $description.'<br/>'.$item['#description'];
          }
        }

        // Save #drup_context for each key
        $this->drupSettings->setContexts($this->formItemsData);

        $this->populateDefaultValues($item, $form_state);
      }
    }
  }

}
