<?php

declare(strict_types=1);

namespace Drupal\drup_push\Routing;

use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;

/**
 * Provides HTML routes for entities with administrative pages.
 */
final class ContentPushHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  //  protected function getCanonicalRoute(EntityTypeInterface $entity_type): ?Route {
  //    return $this->getEditFormRoute($entity_type);
  //  }

}
