<?php

declare(strict_types=1);

namespace Drupal\drup_push;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a content push entity type.
 */
interface ContentPushInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
