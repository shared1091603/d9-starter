<?php

declare(strict_types=1);

namespace Drupal\drup_push\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Content Push type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "content_push_type",
 *   label = @Translation("Content Push type"),
 *   label_collection = @Translation("Content Push types"),
 *   label_singular = @Translation("content push type"),
 *   label_plural = @Translation("content pushes types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count content pushes type",
 *     plural = "@count content pushes types",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\drup_push\Form\ContentPushTypeForm",
 *       "edit" = "Drupal\drup_push\Form\ContentPushTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\drup_push\ContentPushTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer content_push types",
 *   bundle_of = "content_push",
 *   config_prefix = "content_push_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/content_push_types/add",
 *     "edit-form" = "/admin/structure/content_push_types/manage/{content_push_type}",
 *     "delete-form" = "/admin/structure/content_push_types/manage/{content_push_type}/delete",
 *     "collection" = "/admin/structure/content_push_types",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   },
 * )
 */
final class ContentPushType extends ConfigEntityBundleBase {

  /**
   * The machine name of this content push type.
   */
  protected string $id;

  /**
   * The human-readable name of the content push type.
   */
  protected string $label;

}
