<?php

declare(strict_types=1);

namespace Drupal\drup_push\Entity;

use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\drup_push\ContentPushInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the content push entity class.
 *
 * @ContentEntityType(
 *   id = "content_push",
 *   label = @Translation("Content Push"),
 *   label_collection = @Translation("Content Pushes"),
 *   label_singular = @Translation("content push"),
 *   label_plural = @Translation("content pushes"),
 *   label_count = @PluralTranslation(
 *     singular = "@count content pushes",
 *     plural = "@count content pushes",
 *   ),
 *   bundle_label = @Translation("Content Push type"),
 *   handlers = {
 *     "list_builder" = "Drupal\drup_push\ContentPushListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\drup_push\ContentPushAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\drup_push\Form\ContentPushForm",
 *       "edit" = "Drupal\drup_push\Form\ContentPushForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" =
 *   "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *       "revision-delete" =
 *   \Drupal\Core\Entity\Form\RevisionDeleteForm::class,
 *       "revision-revert" =
 *   \Drupal\Core\Entity\Form\RevisionRevertForm::class,
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\drup_push\Routing\ContentPushHtmlRouteProvider",
 *       "revision" =
 *   \Drupal\Core\Entity\Routing\RevisionHtmlRouteProvider::class,
 *     },
 *   },
 *   base_table = "content_push",
 *   data_table = "content_push_field_data",
 *   revision_table = "content_push_revision",
 *   revision_data_table = "content_push_field_revision",
 *   show_revision_ui = TRUE,
 *   translatable = TRUE,
 *   admin_permission = "administer content_push types",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "langcode" = "langcode",
 *     "bundle" = "bundle",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log",
 *   },
 *   links = {
 *     "collection" = "/admin/content/content-push",
 *     "add-form" = "/content-push/add/{content_push_type}",
 *     "add-page" = "/content-push/add",
 *     "canonical" = "/content-push/{content_push}",
 *     "edit-form" = "/content-push/{content_push}/edit",
 *     "delete-form" = "/content-push/{content_push}/delete",
 *     "delete-multiple-form" = "/admin/content/content-push/delete-multiple",
 *     "revision" =
 *   "/content-push/{content_push}/revision/{content_push_revision}/view",
 *     "revision-delete-form" =
 *   "/content-push/{content_push}/revision/{content_push_revision}/delete",
 *     "revision-revert-form" =
 *   "/content-push/{content_push}/revision/{content_push_revision}/revert",
 *     "version-history" = "/content-push/{content_push}/revisions",
 *   },
 *   bundle_entity_type = "content_push_type",
 *   field_ui_base_route = "entity.content_push_type.edit_form",
 * )
 */
final class ContentPush extends RevisionableContentEntityBase implements ContentPushInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setTranslatable(TRUE)
      ->setLabel(t('Label'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Author'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(self::class.'::getDefaultEntityOwner')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the content push was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the content push was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }

}
