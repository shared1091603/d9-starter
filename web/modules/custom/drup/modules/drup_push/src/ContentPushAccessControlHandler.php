<?php

declare(strict_types=1);

namespace Drupal\drup_push;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the content push entity type.
 *
 * phpcs:disable Drupal.Arrays.Array.LongLineDeclaration
 *
 * @see https://www.drupal.org/project/coder/issues/3185082
 */
final class ContentPushAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResult {
    if ($account->hasPermission($this->entityType->getAdminPermission())) {
      return AccessResult::allowed()->cachePerPermissions();
    }

    return match ($operation) {
      'view' => AccessResult::allowedIfHasPermission($account, 'view content_push'),
      'update' => AccessResult::allowedIfHasPermission($account, 'edit content_push'),
      'delete' => AccessResult::allowedIfHasPermission($account, 'delete content_push'),
      'delete revision' => AccessResult::allowedIfHasPermission($account, 'delete content_push revision'),
      'view all revisions', 'view revision' => AccessResult::allowedIfHasPermissions($account, ['view content_push revision', 'view content_push']),
      'revert' => AccessResult::allowedIfHasPermissions($account, ['revert content_push revision', 'edit content_push']),
      default => AccessResult::neutral(),
    };
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL): AccessResult {
    return AccessResult::allowedIfHasPermissions($account, ['create content_push', 'administer content_push types'], 'OR');
  }

}
