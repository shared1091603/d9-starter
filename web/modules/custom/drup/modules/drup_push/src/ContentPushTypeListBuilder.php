<?php

declare(strict_types=1);

namespace Drupal\drup_push;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of content push type entities.
 *
 * @see \Drupal\drup_push\Entity\ContentPushType
 */
final class ContentPushTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['label'] = $this->t('Label');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    $row['label'] = $entity->label();

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render(): array {
    $build = parent::render();

    $build['table']['#empty'] = $this->t(
      'No content push types available. <a href=":link">Add content push type</a>.',
      [':link' => Url::fromRoute('entity.content_push_type.add_form')->toString()],
    );

    return $build;
  }

}
