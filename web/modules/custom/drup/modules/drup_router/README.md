# Drup Router

## Introduction

Ce module permet d'associer de manière arbitraire un slug (ou route name) à une entité.
On ne dépend plus ainsi pour traiter une entité de son id, qui peut changer selon l'environnement.

```php
// La page contact est le node dont l'id est 8

// Methode standard :
$contactUrl = Node::load(8)?->toUrl();

// Avec DrupRouter, on associe une route arbitraire "contact" au node 8.
/** @var \Drupal\drup_router\DrupRouter $drupRouter */
$drupRouter = \Drupal::service('drup.router');
$contactUrl = $drupRouter->getUrl('contact');
```

## Service

```php
/** @var \Drupal\drup_router\DrupRouter $drupRouter */
$drupRouter = \Drupal::service('drup.router');

// Url
$url = $drupRouter->getUrl('news')

// Entity ID
$id = $drupRouter->getId('news');

// Entity
$entity = $drupRouter->getEntity('news');
$title = $drupRouter->getEntity('news')?->getTitle();

// Check if current entity is the specified route
if ($drupRouter->isRoute('news')) {}
```

## Pathauto

Les urls des entités déclarées dans le module sont disponibles en tant que tokens.

```
# Exemple de pattern sans drup_router pour un node "Actualité"
nos-actualites/[node:title]

# Avec le node "Nos actualités" lié à une route "news" :
[drup.router:news]/[node:title]
```
