<?php

namespace Drupal\drup_router;

use Drupal\Core\Language\Language;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Url;
use Drupal\drup\DrupPageEntity;

/**
 * Class DrupRouter
 */
class DrupRouter {

  /**
   * Configuration's name
   *
   * @var string
   */
  public static $configName = 'drup.router';

  /**
   * @var array
   */
  protected $routes;

  /**
   * @var string
   */
  protected $defaultContext;

  /**
   * @var string
   */
  protected $currentLanguageId;

  /**
   * @var \Drupal\drup\DrupPageEntity
   */
  protected $drupPageEntity;

  /**
   * DrupRouterService constructor.
   */
  public function __construct(LanguageManager $languageManager, DrupPageEntity $drupPageEntity) {
    $this->defaultContext = $languageManager->getDefaultLanguage()->getId();
    $this->currentLanguageId = $languageManager->getCurrentLanguage()->getId();

    $this->drupPageEntity = $drupPageEntity;

    $this->routes = \Drupal::config(static::$configName)->get('routes');
  }

  /**
   * Get entity id attached to route name
   *
   * @param $routeName
   * @param  null  $context
   *
   * @return null
   */
  public function getId($routeName, $context = NULL) {
    $context = $this->getContext($context);

    if ($route = $this->getRoute($routeName)) {
      return (string) !empty($route[$context]) ? $route[$context] : $route[$this->defaultContext];
    }

    return NULL;
  }

  /**
   * @param  null  $context
   *
   * @return null|string
   */
  protected function getContext($context = NULL) {
    if ($context === NULL) {
      $context = $this->currentLanguageId;
    }

    return $context;
  }

  /**
   * Return specific route data by name
   *
   * @param $routeName
   *
   * @return null
   */
  public function getRoute($routeName = NULL) {
    if (!empty($this->getRoutes())) {
      if ($routeName === NULL) {
        $routeName = $this->getName();
      }
      foreach ($this->getRoutes() as $route) {
        if ($route['route_name'] === $routeName) {
          return $route;
        }
      }
    }

    return NULL;
  }

  /**
   * @return array
   */
  public function getRoutes() {
    return $this->routes;
  }

  /**
   * Return route name attached to a given drupal entity
   *
   * @param  null  $entityId
   * @param  null  $entityType
   * @param  null  $context
   *
   * @return null
   */
  public function getName($entityId = NULL, $entityType = NULL, $context = NULL) {
    if ($entityId === NULL) {
      $entityId = (string) $this->drupPageEntity->id();
    }
    if ($entityType === NULL) {
      $entityType = $this->drupPageEntity->getEntityType();
    }

    if (!empty($entityId) && !empty($this->getRoutes())) {
      foreach ($this->getRoutes() as $route) {
        if (($route['entity_type'] === $entityType) && ($entityId === $this->getId($route['route_name'], $context))) {
          return $route['route_name'];
        }
      }
    }

    return NULL;
  }

  /**
   * @param $routeName
   * @param  null  $context
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   */
  public function getEntity($routeName, $context = NULL) {
    if (($route = $this->getRoute($routeName)) && $routeEntityId = $this->getId($routeName, $context)) {
      return \Drupal::entityTypeManager()
        ->getStorage($route['entity_type'])
        ->load($routeEntityId);
    }

    return NULL;
  }

  /**
   * Get url alias of given route name
   *
   * @param $routeName
   *
   * @param  null  $context
   *
   * @return null
   */
  public function getPath($routeName, $context = NULL) {
    if (($url = $this->getUrl($routeName, $context)) && $url instanceof Url) {
      return $url->toString();
    }

    return NULL;
  }

  /**
   * Get Url entity from route name
   *
   * @param $routeName
   * @param  null  $context
   *
   * @return \Drupal\Core\Url|null
   */
  public function getUrl($routeName, $context = NULL) {
    $language = new Language(['id' => $this->getContext($context)]);

    if (($uri = $this->getUri($routeName, $context)) && ($url = Url::fromUri($uri, ['language' => $language])) && $url->isRouted()) {
      return $url;
    }

    return NULL;
  }

  /**
   * Get drupal entity uri of a given route name
   *
   * @param $routeName
   *
   * @param  null  $context
   *
   * @return null|string
   */
  public function getUri($routeName, $context = NULL) {
    if (($route = $this->getRoute($routeName)) && $routeEntityId = $this->getId($routeName, $context)) {
      return 'internal:/'.$route['entity_type'].'/'.$routeEntityId;
    }

    return NULL;
  }

  /**
   * @param $routeName
   * @param  null  $context
   *
   * @return bool
   */
  public function isRoute($routeName, $context = NULL) {
    if (($route = $this->getRoute($routeName)) && $routeEntityId = $this->getId($routeName, $context)) {
      return (($this->drupPageEntity->getEntityType() === $route['entity_type']) && ((string) $this->drupPageEntity->id() === $routeEntityId));
    }

    return FALSE;
  }

}
