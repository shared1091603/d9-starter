<?php

namespace Drupal\drup_router\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\drup_router\DrupRouter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DrupRouterForm.
 */
class DrupRouterForm extends ConfigFormBase {

  /**
   * @var string[]
   */
  protected static $defaultAllowedEntitiesType = [
    'node',
  ];

  /**
   * @var \Drupal\Core\Language\LanguageInterface[]
   */
  private array $languages;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager;

  /**
   * @param  \Drupal\Core\Config\ConfigFactoryInterface  $config_factory
   * @param  \Drupal\Core\Entity\EntityTypeManagerInterface  $entityTypeManager
   * @param  \Drupal\Core\Language\LanguageManagerInterface  $languageManager
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entityTypeManager, LanguageManagerInterface $languageManager) {
    parent::__construct($config_factory);

    $this->languages = $languageManager->getLanguages();
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * @inheritDoc
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'drup_router_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $routes = $this->config(DrupRouter::$configName)->get('routes') ?: [];
    $allowedEntitiesType = $this->config(DrupRouter::$configName)
      ->get('allowed_entity_types') ?: static::$defaultAllowedEntitiesType;

    /* Entities
       ========================================================================== */
    $form['allowed_entity_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Allowed entity types'),
      '#required' => TRUE,
      '#options' => [],
      '#default_value' => $allowedEntitiesType,
    ];
    @
    $entityTypesDefinitions = $this->entityTypeManager->getDefinitions();
    foreach ($entityTypesDefinitions as $entity_type_id => $entity_type) {
      if ($entity_type->getGroup() === 'content') {
        $form['allowed_entity_types']['#options'][$entity_type_id] = $entity_type->getLabel()->__toString();
      }
    }

    /* Routes
       ========================================================================== */
    $form['routes'] = [
      '#type' => 'container',
    ];

    $tableHeader = [
      'entity_type' => '',
      'route_name' => $this->t('Route'),
    ];
    foreach ($this->languages as $langcode => $language) {
      $tableHeader[$langcode] = $language->getName();
      $tableHeader[$langcode] .= ' ('.$this->t($language->isDefault() ? 'default' : 'override').')';
    }

    foreach (array_filter(array_values($allowedEntitiesType)) as $index => $entityTypeId) {
      $entityTypeLabel = $entityTypesDefinitions[$entityTypeId]->getLabel()
        ->__toString();
      $entityTypeRoutes = array_filter($routes, static function($route) use ($entityTypeId) {
        return $route['entity_type'] === $entityTypeId;
      });

      $form['routes'][$entityTypeId] = [
        '#type' => 'details',
        '#open' => $index === 0,
        '#title' => $this->t('@type routes', ['@type' => $entityTypeLabel]),
        'routes__'.$entityTypeId => [
          '#type' => 'table',
          '#header' => $tableHeader,
          '#empty' => $this->t('No @type route found', ['@type' => $entityTypeLabel]),
          '#rows' => [],
          '#tabledrag' => [
            [
              'action' => 'order',
              'relationship' => 'sibling',
              'group' => 'row-'.$entityTypeId.'-weight',
            ],
          ],
        ],
      ];
      $index = 0;
      if (!empty($entityTypeRoutes)) {
        foreach ($entityTypeRoutes as $route) {
          if (!empty($route['route_name'])) {
            $form['routes'][$entityTypeId]['routes__'.$entityTypeId][] = $this->setRow($route, $index);
            $index++;
          }
        }
      }
      $form['routes'][$entityTypeId]['routes__'.$entityTypeId][] = $this->setRow([
        'is_new' => TRUE,
        'entity_type' => $entityTypeId,
      ], $index);
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * @param $values
   * @param $index
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function setRow(array $values, $index) {
    $row = [];
    $isNewRow = (isset($values['is_new']) && $values['is_new']);
    $targetEntityTypeStorage = $this->entityTypeManager->getStorage($values['entity_type']);
    $targetTypeLabel = $targetEntityTypeStorage->getEntityType()->getLabel();

    $row['#attributes']['class'][] = 'draggable';

    $row['#weight'] = $index;

    $row['entity_type'] = [
      '#type' => 'hidden',
      '#default_value' => $values['entity_type'],
    ];
    $row['route_name'] = [
      '#type' => 'textfield',
      '#title' => !$isNewRow ? $this->t('Route name') : $this->t('New @type route name', ['@type' => $targetTypeLabel]),
      '#title_display' => !$isNewRow ? 'invisible' : 'before',
      '#placeholder' => !$isNewRow ? $this->t('Route name') : '',
      '#default_value' => !$isNewRow ? $values['route_name'] : NULL,
    ];
    foreach ($this->languages as $langcode => $language) {
      $targetEntity = (isset($values[$langcode])) ? $targetEntityTypeStorage->load($values[$langcode]) : NULL;

      $row[$langcode] = [
        '#type' => 'entity_autocomplete',
        '#target_type' => $values['entity_type'],
        '#title' => $this->t('Entity'),
        '#title_display' => !$isNewRow ? 'invisible' : 'before',
        '#placeholder' => !$isNewRow ? $targetTypeLabel : '',
        '#default_value' => $targetEntity,
      ];
    }

    return $row;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Check if route names are unique
    $routeNames = [];
    foreach ($form_state->getValues() as $formKey => $formValue) {
      if (str_starts_with($formKey, 'routes__')) {
        $routeNames = array_merge($routeNames, array_column($formValue, 'route_name'));
      }
    }
    $routeNames = array_filter($routeNames);
    if (count($routeNames) !== count(array_unique($routeNames))) {
      $form_state->setErrorByName('routes', $this->t('Route names have to be unique.'));
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $routes = [];

    foreach ($form_state->getValues() as $formKey => $formValue) {
      if (str_starts_with($formKey, 'routes__')) {
        $routes = array_merge($routes, $formValue);
      }
    }
    $routes = array_filter($routes, static function($route) {
      return !empty($route['route_name']);
    });

    $this->config(DrupRouter::$configName)
      ->set('routes', $routes)
      ->set('allowed_entity_types', $form_state->getValue('allowed_entity_types'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      DrupRouter::$configName,
    ];
  }

}
