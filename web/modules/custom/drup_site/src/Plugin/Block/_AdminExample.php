<?php

namespace Drupal\drup_site\Plugin\Block;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;
use Drupal\drup\Block\DrupBlockAdminBase;
use Drupal\drup_settings\DrupSettings;

/**
 * Provides a '_AdminExample' block.
 *
 * @Block(
 *  id = "admin_example",
 *  admin_label = @Translation("Admin Example"),
 * )
 */
class _AdminExample extends DrupBlockAdminBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function setAjaxRow(&$rowContainer, $rowValues) {
    if (!empty($rowValues['title'])) {
      $rowContainer['#title'] = $rowValues['title'];
    }
    $rowContainer['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#required' => TRUE,
      '#default_value' => !empty($rowValues['title']) ? $rowValues['title'] : NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $form['title'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Title'),
      '#rows' => 2,
      '#default_value' => !empty($this->configValues['title']) ? $this->configValues['title'] : NULL,
    ];

    $this->ajaxMaxRows = 5;
    $this->buildAjaxContainer($form, $form_state);

    // Update wordings
    $form[$this->ajaxContainer]['values']['#header'][0] = $this->t('Contenus');
    $form[$this->ajaxContainer]['values']['actions']['submits']['add_item']['#value'] = $this->t('Ajouter un contenu');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setCustomConfigValue($this->ajaxContainer, $form_state->getValue($this->ajaxContainer));
    parent::blockSubmit($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    parent::build();

    $values = $this->getCustomConfigValues();

    $items = [];

    $build = $this->mergeBuildParameters([
      '#theme' => 'backend_example',
      '#title' => $values['title'],
      '#items' => $items,
    ]);

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), []);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return Cache::mergeTags(parent::getCacheTags(), DrupSettings::getCacheTags());
  }

}
