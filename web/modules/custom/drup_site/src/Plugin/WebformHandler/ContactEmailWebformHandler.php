<?php

namespace Drupal\drup_site\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\drup_site\Utility\FormUtility;
use Drupal\webform\Plugin\WebformHandler\EmailWebformHandler;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Emails a webform submission.
 *
 * @WebformHandler(
 *   id = "dynamic_contact_email_recipient",
 *   label = @Translation("Email - Destinataire administrable et dynamique
 *   selon sur le sujet"), category = @Translation("Notification"), description
 *   = @Translation("Le destinataire sera fonction du sujet du formulaire, et
 *   modifiable dans la Configuration du site"), cardinality =
 *   \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results =
 *   \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission =
 *   \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 *   tokens = TRUE,
 * )
 */
class ContactEmailWebformHandler extends EmailWebformHandler {

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    //$form['to']['#access'] = FALSE;
    $form['to']['#description'] = t('Override dans la Configuration du site');

    return $form;
  }

  public function sendMessage(WebformSubmissionInterface $webform_submission, array $message) {
    if ($email = FormUtility::getRecipientEmail($webform_submission)) {
      $message['to_mail'] = $email;
    }

    parent::sendMessage($webform_submission, $message);
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return [
        '#theme' => 'webform_handler_'.'email'.'_summary',
      ] + parent::getSummary();
  }

}
