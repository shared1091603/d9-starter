<?php

namespace Drupal\drup_site\Utility;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Pager\PagerParametersInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\drup\Entity\EntityField;
use Drupal\drup\Helper\DrupRequest;
use Drupal\drup_settings\DrupSettings;

/**
 * Class DrupSEO
 *
 * @package Drupal\drup
 */
abstract class SEOUtility {

  /**
   * Nom du groupe contenant les tokens SEO
   *
   * @var string
   */
  public static $tokenType = 'seo';

  /**
   * Nom de l'image style utilisé pour les images en SEO
   *
   * @var string
   */
  public static $imageStyle = 'large';

  /**
   * Déclaration des tokens pour le SEO
   *
   * @param $info
   */
  public static function hook_token_info(&$info) {
    // Déclaration du groupe SEO
    $info['types'][self::$tokenType] = [
      'name' => 'SEO',
      'description' => 'Données utilisées pour les métas SEO',
      //'needs-data' => 'node' // Ajout l'objet Node si on est sur un node
    ];

    // Meta title/desc
    $info['tokens'][self::$tokenType]['meta:title'] = [
      'name' => 'Meta "title" automatique',
    ];
    $info['tokens'][self::$tokenType]['meta:desc'] = [
      'name' => 'Meta "description" automatique',
    ];
    $info['tokens']['seo']['meta:image'] = [
      'name' => 'Meta "image url" automatique',
    ];
  }

  /**
   * Contenu des tokens
   *
   * @param $replacements
   * @param $type
   * @param  array  $tokens
   * @param  array  $data
   * @param  array  $options
   * @param  \Drupal\Core\Render\BubbleableMetadata  $bubbleable_metadata
   */
  public static function hook_tokens($type, array $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
    $replacements = [];

    if ($type === self::$tokenType) {
      /** @var DrupSettings $drupSettings */
      $drupSettings = \Drupal::service('drup.settings');
      /** @var \Drupal\metatag\MetatagManager $metatagManager */
      $metatagManager = \Drupal::service('metatag.manager');
      /** @var \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository */
      $entityRepository = \Drupal::service('entity.repository');

      /** @var \Drupal\pathauto\AliasCleaner $cleaner */
      $cleaner = \Drupal::service('pathauto.alias_cleaner');

      if (empty($options['langcode'])) {
        $options['langcode'] = \Drupal::languageManager()->getCurrentLanguage()->getId();
      }

      $entityField = NULL;
      $entity = current($data);
      if ($entity instanceof \Drupal\Core\Entity\ContentEntityInterface) {
        $entity = $entityRepository->getTranslationFromContext($entity, $options['langcode']);
        $entityField = new EntityField($entity);
      }

      // Tokens
      foreach ($tokens as $name => $original) {
        /* TITLE
           ========================================================================== */
        if ($name === 'meta:title') {
          $title = NULL;

          //if () {
          //
          //}
          //else {
          if ($entity instanceof ContentEntityInterface) {
            $tags = $metatagManager->tagsFromEntity($entity);
            $title = empty($tags['title']) ? $entity?->label() : $tags['title'];
          }

          if (!empty($title)) {
            $replacements[$original] = $title;
          }
        }
        /* DESC
           ========================================================================== */
        elseif ($name === 'meta:desc') {
          $description = NULL;
          $tags = $entity instanceof ContentEntityInterface ? $metatagManager->tagsFromEntity($entity) : [];

          if (empty($tags['description'])) {
            if ($entity && $fieldSubtitle = ContentUtility::getSubtitle($entity, FALSE)) {
              $description = strip_tags($fieldSubtitle);
            }
          }

          if (!empty($description)) {
            $replacements[$original] = Unicode::truncate($description, 255, TRUE, TRUE);
          }
          elseif (isset($tags['description'])) {
            $replacements[$original] = $tags['description'];
          }
        }
        /* IMAGE
           ========================================================================== */
        elseif ($name === 'meta:image') {
          $url = NULL;
          $imageStyle = 'large';

          if ($entity) {
            if ($mediaBanner = ContentUtility::getBanner($entity)) {
              $url = $mediaBanner->getThumbnailFile()?->getUrl($imageStyle);
            }
            elseif ($mediaThumbnail = ContentUtility::getThumbnail($entity)) {
              $url = $mediaThumbnail->getThumbnailFile()?->getUrl($imageStyle);
            }
          }

          if (!empty($url)) {
            $replacements[$original] = $url;
          }
          else {
            $replacements[$original] = '/'.\Drupal::theme()->getActiveTheme()->getPath().'/logo-social.png';
          }
        }
      }
    }

    return $replacements;
  }

  /**
   * Tokens alter
   *
   * @param $replacements
   * @param $context
   */
  public static function hook_tokens_alter(&$replacements, &$context) {
    if ($context['type'] === self::$tokenType) {
      $tokens = [
        'meta:title',
        'meta:desc',
      ];

      /** @var PagerParametersInterface $pagerParameters */
      $pagerParameters = \Drupal::service('pager.parameters');
      $page = $pagerParameters->findPage();

      foreach ($tokens as $token) {
        $tokenMeta = '['.$context['type'].':'.$token.']';

        if (isset($context['tokens'][$token], $replacements[$tokenMeta])) {
          // Page number
          if ($page > 0) {
            $replacements[$tokenMeta] .= ' - '.t('Page').' '.($page + 1);
          }

          // Site title
          if ($token === 'meta:title') {
            $replacements[$tokenMeta] .= ' | '.DrupRequest::getSiteName();
          }
          elseif ($token === 'meta:desc') {
            $replacements[$tokenMeta] .= ' - '.DrupRequest::getSiteName();
          }
        }
      }
    }
  }

  /**
   * @param  array  $attachments
   *
   * @return array
   */
  public static function hook_page_attachments_alter(array &$attachments) {
    foreach ($attachments['#attached']['html_head'] as $index => &$attachment) {
      $attachmentId = $attachment[1];
      $attachmentData = $attachment[0];

      // Suppression des certaines méta inutiles
      if (in_array($attachmentId, ['system_meta_generator'])) {
        unset($attachments['#attached']['html_head'][$index]);
      }

      // Demande interne : "Supprimer les canonicals sur les pages classiques de contenus"
      // = enlever la canonique si elle correspond à l'url actuelle
      // on la conserve si url différente, et/ou si paramètres GET
      elseif ($attachmentId === 'canonical_url' && isset($attachmentData['#attributes']['href'])) {
        if ($attachmentData['#attributes']['href'] === \Drupal::request()
            ->getUri()) {
          unset($attachments['#attached']['html_head'][$index]);
        }
      }
    }
    unset($attachment);

    return $attachments;
  }

  /**
   * @param  array  $variables
   * @param  array  $data
   * @param  int  $version
   * @param  null  $path
   */
  public static function addFavicons(array &$variables, array $data = [], int $version = 1, $path = NULL) {
    if (empty($path)) {
      $path = \Drupal::request()->getBaseUrl().'/'.\Drupal::theme()
          ->getActiveTheme()
          ->getPath().'/dist/images/favicons';
    }
    $siteName = \Drupal::config('system.site')->get('name');

    $metas = [
      'shortcut' => [
        '#tag' => 'link',
        '#attributes' => [
          'rel' => 'shortcut icon',
          'href' => $path.'/favicon.ico?v='.$version,
        ],
      ],
      'apple-touch-icon' => [
        '#tag' => 'link',
        '#attributes' => [
          'rel' => 'apple-touch-icon',
          'sizes' => '180x180',
          'href' => $path.'/apple-touch-icon.png?v='.$version,
        ],
      ],
      'icon32x32' => [
        '#tag' => 'link',
        '#attributes' => [
          'rel' => 'icon',
          'sizes' => '32x32',
          'href' => $path.'/favicon-32x32.png?v='.$version,
        ],
      ],
      'icon16x16' => [
        '#tag' => 'link',
        '#attributes' => [
          'rel' => 'icon',
          'sizes' => '16x16',
          'href' => $path.'/favicon-16x16.png?v='.$version,
        ],
      ],
      'manifest' => [
        '#tag' => 'link',
        '#attributes' => [
          'rel' => 'manifest',
          'href' => $path.'/site.webmanifest?v='.$version,
        ],
      ],
      'mask-icon' => [
        '#tag' => 'link',
        '#attributes' => [
          'rel' => 'mask-icon',
          'href' => $path.'/safari-pinned-tab.svg?v='.$version,
          'color' => $data['color_mask_icon'],
        ],
      ],
      'apple-mobile-web-app-title' => [
        '#tag' => 'meta',
        '#attributes' => [
          'name' => 'apple-mobile-web-app-title',
          'content' => $siteName,
        ],
      ],
      'msapplication-TileColor' => [
        '#tag' => 'meta',
        '#attributes' => [
          'name' => 'msapplication-TileColor',
          'content' => $data['color_msapplication'],
        ],
      ],
      'msapplication-config' => [
        '#tag' => 'meta',
        '#attributes' => [
          'name' => 'msapplication-config',
          'content' => $path.'/browserconfig.xml?v='.$version,
        ],
      ],
      'theme-color' => [
        '#tag' => 'meta',
        '#attributes' => [
          'name' => 'theme-color',
          'content' => $data['color_theme'],
        ],
      ],
    ];

    foreach ($metas as $metaName => $meta) {
      $variables['page']['#attached']['html_head'][] = [
        $meta,
        'favicons-'.$metaName,
      ];
    }
  }

}
