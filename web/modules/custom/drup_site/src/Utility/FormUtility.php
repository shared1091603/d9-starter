<?php

namespace Drupal\drup_site\Utility;

use Drupal\Core\Form\FormStateInterface;
use Drupal\drup_settings\DrupSettings;
use Drupal\webform\Entity\Webform;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Class MenuUtility
 *
 * @package Drupal\drup_site\Utility
 */
abstract class FormUtility {

  /**
   * Récupération dynamique du destinataire selon les chamsp remplis dans les
   * Webforms
   *
   * @param  \Drupal\webform\WebformSubmissionInterface  $webform_submission
   *
   * @return string|null
   */
  public static function getRecipientEmail(WebformSubmissionInterface $webform_submission) {
    $webformId = $webform_submission->getWebform()->id();
    $submissionData = $webform_submission->getRawData();
    $webformInfo = self::getWebformInfo($webformId);

    $DrupSettings = \Drupal::service('drup.settings');

    // Récup auto de l'email attribué au sujet du webform dans DrupSettings
    if (($subject = $webform_submission->getElementData('subject')) && $email = self::getDrupSettingsEmailValue($webformId, 'subjects', $subject, '_other_')) {
      return $email;
    }

    return NULL;
  }

  public static function getWebformInfo($webform_id) {
    return self::getWebformsInfo()[$webform_id] ?? NULL;
  }

  /**
   * Liste des configs des Webforms + support pour configurer les mails
   * destinataire dans DrupSettings
   *
   * @return array
   */
  public static function getWebformsInfo() {
    $data = [
      'contact' => [
        'subjects' => [
          "subject_a" => t("Subject A", [], ['context' => 'form_subjects_contact']),
          "subject_b" => t("Subject B", [], ['context' => 'form_subjects_contact']),
          "_other_" => t("Other (please specify)", [], ['context' => 'form_subjects_contact']),
          // _other_ = clé du champ "Select autre"
        ],
      ],
    ];

    foreach ($data as $webform_id => &$webform_data) {
      $webform_data['webform'] = Webform::load($webform_id);
    }

    return $data;
  }

  /**
   * @param $webform_id
   * @param $namespace
   * @param $field_value
   * @param $field_value_default
   *
   * @return mixed
   */
  public static function getDrupSettingsEmailValue($webform_id, $namespace, $field_value, $field_value_default = NULL) {
    $DrupSettings = \Drupal::service('drup.settings');

    $value = $DrupSettings->getValue('webform_'.$webform_id.'__'.$namespace.'__'.$field_value);

    if (empty($value) && !empty($field_value_default)) {
      $value = $DrupSettings->getValue('webform_'.$webform_id.'__'.$namespace.'__'.$field_value_default);
    }

    return $value;
  }

  /**
   * Ajoute les configs d'emails destinataies des formulaires pour DrupSettings
   *
   * @param  array  $form
   * @param  \Drupal\Core\Form\FormStateInterface  $form_state
   * @param $container
   *
   * @return void
   */
  public static function alterDrupSettingsForm(array &$form, FormStateInterface $form_state, $container) {
    foreach (self::getWebformsInfo() as $webform_id => $webform_data) {
      $key = 'webform_'.$webform_id;

      if ($webform_data['webform'] instanceof Webform) {
        $subjects = $webform_data['subjects'] ?? ['default' => t('Default')];
        $form[$container]['emails'][$key] = [
          '#type' => 'details',
          '#open' => TRUE,
          //          '#title' => t('@title form: Recipient email addresses', ['@title' => $webform_data['webform']->label()]),
          '#title' => t('Formulaire "@title"', ['@title' => $webform_data['webform']->label()]),
        ];

        //        // Spécifique Contact :
        //        if ($webform_id === 'contact') {
        //          $form[$container]['emails'][$key]['brands'] = [
        //            '#type' => 'details',
        //            '#title' => t('Si un internaute provient d\'un CTR de contact d\'une page marque'),
        //            '#open' => FALSE,
        //          ];
        //          foreach (RouterUtility::$subsidiaries as $index => $brand) {
        //            $form[$container]['emails'][$key]['brands'][$key . '__brands__' . $brand] = [
        //              '#type' => 'email',
        //              '#title' => ucfirst($brand),
        //              '#drup_context' => DrupSettings::$contextNeutral,
        //              '#wrapper_attributes' => ['class' => ['container-inline']],
        //            ];
        //          }
        //        }

        $form[$container]['emails'][$key]['subjects'] = [
          '#type' => 'details',
          '#open' => FALSE,
          '#title' => t('Choix "Sujet"'),
        ];
        foreach ($subjects as $subject_id => $subject_label) {
          $form[$container]['emails'][$key]['subjects'][$key.'__subjects__'.$subject_id] = [
            '#type' => 'email',
            '#title' => $subject_label,
            '#drup_context' => DrupSettings::$contextNeutral,
            '#wrapper_attributes' => ['class' => ['container-inline']],
          ];
        }
      }
    }
  }

  /**
   * Label de la checkbox RGPD selon le type de formulaire
   *
   * @param  string  $type
   *
   * @return string
   */
  public static function getGDPRCheckboxLabel(string $type = 'contact') {
    $drupRouter = \Drupal::service('drup.router');

    $text = '';

    switch ($type) {
      default:
        $text .= t('<span class="form-rgpd-text">Vous acceptez que les informations saisies soient exploitées dans le cadre du traitement de votre demande et de la relation qui peut en découler. Vous bénéficiez d\'un droit d’accès, de rectification et d\'opposition. Pour plus d\'information sur vos droits informatique & Liberté <a href=":url" target="_blank">cliquez ici</a>.</span>', [
          ':url' => $drupRouter->getPath('privacy'),
        ]);
        break;
    }

    return $text;
  }

}
