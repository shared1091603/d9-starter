<?php

namespace Drupal\drup_site\Utility;

use Drupal\drup_router\DrupRouter;

/**
 * Méthodes communes pour identifier les pages
 */
class RouterUtility {

  /**
   * [BUNDLE] => [ROUTE]
   *
   * @var string[]
   */
  public static $entityMainRoute = [

  ];

  /**
   * @param  \Drupal\Core\Entity\ContentEntityBase  $entity
   * @param  \Drupal\drup_router\DrupRouter|NULL  $drupRouter
   * @param  string|NULL  $context
   *
   * @return \Drupal\Core\Url|null
   */
  public static function getBackPageEntity(\Drupal\Core\Entity\ContentEntityBase $entity, DrupRouter $drupRouter = NULL, string $context = NULL) {
    if (isset(static::$entityMainRoute[$entity->bundle()])) {
      return self::getDrupRouter($drupRouter)?->getUrl(static::$entityMainRoute[$entity->bundle()], $context);
    }

    return NULL;
  }

  /**
   * @param  DrupRouter|null  $drupRouter
   *
   * @return DrupRouter
   */
  protected static function getDrupRouter(DrupRouter $drupRouter = NULL) {
    if (!$drupRouter instanceof DrupRouter) {
      /** @var DrupRouter $drupRouter */
      $drupRouter = \Drupal::service('drup.router');
    }

    return $drupRouter;
  }

}
