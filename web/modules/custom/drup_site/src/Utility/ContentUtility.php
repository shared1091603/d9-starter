<?php

namespace Drupal\drup_site\Utility;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\drup\Entity\Media;
use Drupal\drup\Entity\Node;
use Drupal\field\FieldConfigInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\ParagraphInterface;

/**
 * Class MenuUtility
 *
 * @package Drupal\drup_site\Utility
 */
abstract class ContentUtility {

  public static $fieldThumbnail = 'field_thumbnail';

  public static $fieldBanner = 'field_banner';

  public static $fieldParagraphs = 'field_paragraph';

  /**
   * @param  \Drupal\Core\Entity\EntityInterface|NULL  $entity
   * @param  bool  $as_markup
   *
   * @return array|\Drupal\Component\Render\MarkupInterface|string|null
   */
  public static function getSubtitle(EntityInterface $entity = NULL, bool $as_markup = TRUE) {
    if ($entity instanceof Node && $content = $entity->entityField()->getProcessedText('subtitle')) {
      return $as_markup ? current($content) : nl2br(current($content)['#text']);
    }

    return NULL;
  }

  /**
   * @param  \Drupal\Core\Entity\EntityBase|NULL  $entity
   *
   * @return \Drupal\drup\Entity\Media|null
   */
  public static function getBanner(EntityBase $entity = NULL) {
    if ($entity instanceof ContentEntityBase && $entity->hasField(self::$fieldBanner) && $medias = $entity->get(self::$fieldBanner)?->referencedEntities()) {
      return current($medias)->translate();
    }

    return NULL;
  }

  /**
   * @param  \Drupal\Core\Entity\EntityBase|NULL  $entity
   *
   * @return bool
   */
  public static function hasBanner(EntityBase $entity = NULL) {
    return ($entity instanceof ContentEntityBase && $entity->hasField(static::$fieldBanner) && !$entity->get(static::$fieldBanner)->isEmpty());
  }

  /**
   * @param  EntityBase|NULL  $entity
   *
   * @return \Drupal\drup\Entity\Media|null
   */
  public static function getThumbnail(EntityBase $entity = NULL) {
    if ($entity instanceof ContentEntityBase && $entity->hasField(self::$fieldThumbnail) && $medias = $entity->get(self::$fieldThumbnail)?->referencedEntities()) {
      return current($medias)->translate();
    }

    return NULL;
  }

  /**
   * Premiers mots des paragraphes de type text
   *
   * @param  \Drupal\Core\Entity\EntityBase|NULL  $entity
   * @param  int  $max_length
   *
   * @return string|null
   */
  public static function getSummary(EntityBase $entity = NULL, int $max_length = 150) {
    $output = NULL;

    if ($entity instanceof ContentEntityBase && $entity->hasField(self::$fieldParagraphs) && !$entity->get(self::$fieldParagraphs)->isEmpty()) {
      /** @var \Drupal\paragraphs\Entity\Paragraph[] $paragraphs */
      $paragraphs = $entity->get(self::$fieldParagraphs)->referencedEntities();
      $texts = [];

      foreach ($paragraphs as $paragraph) {
        $texts = array_merge($texts, self::getParagraphTexts($paragraph));
      }

      if (!empty($texts)) {
        $text = implode(' ', $texts);
        $text = html_entity_decode($text);
        $output = \Drupal\Component\Utility\Unicode::truncate($text, $max_length, TRUE, TRUE);
      }
    }

    return $output;
  }

  /**
   * Retrieves the text values for specified fields within a Paragraph entity.
   *
   * @param  \Drupal\paragraphs\Entity\Paragraph  $paragraph
   *   The Paragraph entity from which to retrieve the text values.
   *
   * @return array
   *   An array of text values extracted from the Paragraph entity fields.
   */
  protected static function getParagraphTexts(Paragraph $paragraph) {
    $output = [];

    $text_types = [
      'text_with_summary',
      'text',
      'text_long',
    ];

    $paragraph = \Drupal::service('entity.repository')->getTranslationFromContext($paragraph);

    $components = \Drupal::service('entity_display.repository')->getFormDisplay('paragraph', $paragraph->getType())->getComponents();
    uasort($components, 'Drupal\Component\Utility\SortArray::sortByWeightElement');

    foreach ($components as $field_name => $field_config) {
      // Components can be extra fields, check if the field really exists.
      if (!$paragraph->hasField($field_name)) {
        continue;
      }

      // Check acccess
      $field_definition = $paragraph->getFieldDefinition($field_name);
      if (!($field_definition instanceof FieldConfigInterface) || !$paragraph->get($field_name)->access('view')) {
        continue;
      }

      // If type text, get value
      if (in_array($field_definition->getType(), $text_types) && !$paragraph->get($field_name)->isEmpty()) {
        $output[] = trim(strip_tags($paragraph->get($field_name)->value));
      }
      // If nested paragraph, loop
      elseif ($field_definition->getType() === 'entity_reference_revisions') {
        foreach ($paragraph->get($field_name) as $item) {
          $paragraph_child = $item->entity;

          if ($paragraph_child instanceof ParagraphInterface) {
            $output = array_merge($output, (self::getParagraphTexts($paragraph_child)));
          }
        }
      }
    }

    return $output;
  }

  /**
   * @return \Drupal\drup\Entity\Media|null
   */
  public static function getPlaceholderThumbnail() {
    // Image placeholder (png transparent)
    if (($mid = \Drupal::service('drup.settings')->getValue('default_list_image')) && $media = Media::load($mid)) {
      return $media;
    }

    return NULL;
  }
  //  public static function getPlaceholderThumbnail(EntityBase $entity = NULL, string $view_mode = 'default') {
  //    if ($entity instanceof ContentEntityBase && $entity->hasField(self::$fieldThumbnail)) {
  //      // Image placeholder (png transparent)
  //      if (($mid = \Drupal::service('drup.settings')->getValue('default_list_image')) && $media = Media::load($mid)) {
  //        /** @var \Drupal\Core\Entity\Entity\EntityViewDisplay $view_display */
  //        $view_display = \Drupal::entityTypeManager()->getStorage('entity_view_display')->load('node.'.$entity->bundle().'.'.$view_mode);
  //        $field_image_style = $view_display->getComponent(self::$fieldThumbnail)['settings']['image_style'];
  //
  //        return $media->renderImage($field_image_style);
  //      }
  //    }
  //
  //    return NULL;
  //  }

}
