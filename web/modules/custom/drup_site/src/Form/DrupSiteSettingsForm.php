<?php

namespace Drupal\drup_site\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Locale\CountryManager;
use Drupal\drup_settings\Form\DrupSettingsForm;
use Drupal\drup_site\Utility\FormUtility;

/**
 * Class DrupSiteSettingsForm
 *
 * @package Drupal\drup_site\Form
 */
class DrupSiteSettingsForm extends DrupSettingsForm {

  /**
   * Url vers les réseaux sociaux (dans l'ordre d'apparition)
   *
   * @var string[]
   */
  public static $socialNetworks = [
    'fb' => 'Facebook',
    'tw' => 'Twitter',
    'yu' => 'Youtube',
  ];

  /**
   * Chaque form item doit :
   * - préciser une clé '#drup_context' => 'und'|null (null pour la langue
   * courante)
   * - ne pas inclure de '#default_value
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    /**
     * CONTACT
     */
    $form[$this->formContainer]['contact'] = [
      '#type' => 'details',
      '#title' => $this->t('Coordonnées'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];

    $form[$this->formContainer]['contact']['contact_infos'] = [
      '#type' => 'details',
      '#title' => $this->t('Informations de contact'),
      '#description' => $this->t('Génère les données structurées Schema.org relatives à votre organisation'),
      '#open' => TRUE,
    ];
    $form[$this->formContainer]['contact']['contact_infos']['contact_infos_company'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Company'),
      '#drup_context' => 'und',
    ];
    $form[$this->formContainer]['contact']['contact_infos']['contact_infos_phone_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('N° téléphone'),
      '#drup_context' => 'und',
    ];

    $form[$this->formContainer]['contact']['contact_infos']['contact_infos_address'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Address'),
      '#rows' => 2,
      '#drup_context' => 'und',
    ];
    $form[$this->formContainer]['contact']['contact_infos']['contact_infos_zipcode'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Zip code'),
      '#size' => 10,
      '#drup_context' => 'und',
    ];
    $form[$this->formContainer]['contact']['contact_infos']['contact_infos_city'] = [
      '#type' => 'textfield',
      '#title' => $this->t('City'),
      '#drup_context' => 'und',
    ];
    $form[$this->formContainer]['contact']['contact_infos']['contact_infos_country'] = [
      '#type' => 'select',
      '#title' => $this->t('Country'),
      '#options' => CountryManager::getStandardList(),
      '#drup_context' => 'und',
    ];
    // ...

    //
    FormUtility::alterDrupSettingsForm($form, $form_state, $this->formContainer);

    // Set #default_values for form items with '#drup_context' keys
    $this->populateDefaultValues($form, $form_state);

    return $form;
  }

}
