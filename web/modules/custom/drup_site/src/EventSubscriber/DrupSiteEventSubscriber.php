<?php

namespace Drupal\drup_site\EventSubscriber;

use Drupal\Core\Url;
use Drupal\drup\Entity\Term;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class CustomRedirects
 *
 * @package Drupal\MYMODULE\EventSubscriber
 *
 * Redirects tags to the projects page with correct filter enabled
 */
class DrupSiteEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['checkForRedirection', 29];

    return $events;
  }

  /**
   * Redirige tous les taxonomy/term/X
   *
   * @link https://stefvanlooveren.me/blog/redirect-taxonomy-terms-filtered-view-page
   *
   * @param  \Symfony\Component\HttpKernel\Event\RequestEvent  $event
   *
   * @return void
   */
  public function checkForRedirection(RequestEvent $event) {
    $request = $event->getRequest();
    $path = $request->getRequestUri();

    // Term de taxo à rediriger
    if ((str_contains($path, 'taxonomy/term')) && (!str_contains($path, '/edit')) && (!str_contains($path, '/delete')) && !str_contains($path, '/translat')) {
      // Redirect tags to project page
      $id = preg_replace("/[^0-9]/", "", $path);
      $term = Term::load($id);
      $vocabulary_id = $term->bundle();

      $newPath = NULL;
      switch ($vocabulary_id) {
        //        case 'sector':
        //          $newPath = '/projecten/overzicht?f[0]=sectors%3A' . $id;
        //          break;
        //        case 'subsidy_framework':
        //          $newPath = '/projecten/overzicht?f[0]=subsidy_frameworks%3A' . $id;
        //          break;
        //        case 'application':
        //          $newPath = '/projecten/overzicht?f[0]=applications%3A' . $id;
        //          break;
        default:
          $newPath = Url::fromRoute('<front>');
      }

      if ($newPath) {
        if ($newPath instanceof Url) {
          $newPath = $newPath->setAbsolute(FALSE)->toString();
        }
        $new_response = new RedirectResponse($newPath, '301');
        $new_response->send();
      }
    }
  }

}
