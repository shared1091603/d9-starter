Add GoogleSheetID to settings.php :

```php
$settings['drup_csv2po']['csv_remote_url'] = 'https://docs.google.com/spreadsheets/d/[ID]/gviz/tq?tqx=out:csv';
$settings['drup_csv2po']['extension_type'] = 'module';
$settings['drup_csv2po']['extension_name'] = 'drup_translations';
```

Commande Make :

```shell
  [lando] make translate
```

Commande Drush d'import complète :

```shell
  lando drush csv2po
  lando drush locale:check
  lando drush locale:update
  lando drush cr
```
